# CNA2 - Cloud-native Advanced 2

- Bereich: Entwicklungsmethoden zur Lösung von ICT-Problemen und Entwicklung von ICT-Innovationen zielführend einsetzen, Cloud-native
- Semester: 5

## Lektionen

* Präsenzlektionen (Vor Ort): 40
* Präsenzlektionen (Distance Learning): 10
* Selbststudium: 10

### Fahrplan

| **Lektionen** | **Selbststudium** | **Inhalt**                             | **Kompetenzenband**        | **Tools**                                 |
|---------------|-------------------|----------------------------------------|----------------------------|-------------------------------------|
| 12            | 8                 | Monitoring                             | f)                         | Prometheus, Grafana, Kubernetes-Dashboard      |
| 10            | 6                 | Logging (Logfiles sammeln, filtern und auswerten) | g)              | Fluentd, ELK Stack (Elasticsearch, Logstash, Kibana) |
| 10            | 6                 | Service Mesh                           | h)                         | Istio, Linkerd, Kubernetes                     |
| 10            | 6                 | Serverless (FaaS)                      | i)                         | OpenFaaS, Knative, Kubernetes                  |
| 8             | 4                 | Network Policies                       | j)                         | Kubernetes Network Policies, Kubectl, YAML     |
| **50**        | **30**            | **Total**                              |                            |                                                |

## Voraussetzungen

* Cloud-native Core

## Dispensation 

* keine

## Methoden

Praktische Laborübungen mit Coaching durch Lehrperson

## Schlüsselbegriffe

CNCF, Monitoring, Logging, Serverless, Operator Pattern, Function as a Service, Helm, Cloud-native, Service Mesh, Network policies

## Lerninhalte

* Versteht die Wichtigkeit und Methoden des Monitorings zur Überwachung und Analyse des Verhaltens und der Leistung von Cloud-nativen Anwendungen.
* Kennt die Techniken und Werkzeuge für das Logging zur Erfassung, Speicherung und Analyse von Protokolldaten in Cloud-nativen Umgebungen.
* Kann eine Logging-Strategie für in Cloud-nativ Umgebung definieren und konfigurieren.
* Versteht die Konzepte und Vorteile eines Service Mesh zur Verbesserung der Kommunikation, Sicherheit und Beobachtbarkeit zwischen Diensten in Cloud-nativen Architekturen
* Kennt die Grundlagen und Einsatzszenarien von Serverless Computing (Function as a Service, FaaS) zur effizienten Ausführung von Code ohne direkte Serververwaltung.
* Kann Serverless Computing Services definieren und konfigurieren. 
* Versteht die Bedeutung und Anwendung von Network Policies zur Gewährleistung einer sicheren Netzwerkkommunikation zwischen Diensten in einer Cloud-nativen Umgebung.
* Kann Network Policies in einer Cloud-native Umgebung definieren und konfigurieren.

## Übungen und Praxis

- **Monitoring**
  - Einrichtung von Grundmonitoring für Pods/Services
  - Visualisierung von Monitoring-Daten mit Grafana
- **Logging**
  - Konfiguration von zentralisiertem Logging für einen Cluster
  - Analyse von Log-Daten mit Kibana
- **Service Mesh**
  - Installation und Konfiguration eines Service Mesh (z.B. Istio)
  - Routing-Regeln und Resilience-Tests im Service Mesh
- **Serverless (Function as a Service, FaaS)**
  - Erstellung und Deployment einer einfachen Funktion mit FaaS
  - Integration einer Serverless-Funktion in eine bestehende Anwendung
- **Network Policies**
  - Definition einer Network Policy zur Isolation von Pods
  - Test der Kommunikationsbeschränkungen zwischen Services

## Lehr- und Lernformen

Vorträge, Lehrgespräche, Workshop, Einzel- und Gruppenarbeiten, Lernfragen, Präsentationen

## Lehrmittel

* Kubernetes Cluster
* CNCF Projects








