# CNA1 - Cloud-native Advanced 1

- Bereich: Entwicklungsmethoden zur Lösung von ICT-Problemen und Entwicklung von ICT-Innovationen zielführend einsetzen, Cloud-native
- Semester: 4

## Lektionen

* Präsenzlektionen (Vor Ort): 30
* Präsenzlektionen (Distance Learning): 0
* Selbststudium: 10

### Fahrplan

| **Lektionen** | **Selbststudium** | **Inhalt**                             | **Kompetenzenband**           | **Tools**                                      |
|---------------|-------------------|----------------------------------------|-------------------------------|---------------------------------|
| 6             | 2                 | Health Probe Pattern                   | a)                            | Kubernetes-Dashboard, Prometheus, Grafana      |
| 6             | 2                 | Init Container                         | b)                            | Kubernetes, Docker, Container-Orchestrierung   |
| 5             | 1.5               | Zugriffssteuerung (RBAC)               | c)                            | Kubernetes RBAC-Tools, Kubectl, YAML           |
| 7             | 2.5               | Helm - Paketmanager für Kubernetes     | d)                            | Helm, Kubernetes, CI/CD-Pipelines              |
| 6             | 2                 | Operator Pattern                       | e)                            | Kubernetes-Operatoren, Operator SDK, Ansible   |
| **30**        | **10**            | **Total**                              |                               |                                                |

## Voraussetzungen

* Cloud-native Core

## Dispensation 

* keine

## Methoden

Praktische Laborübungen mit Coaching durch Lehrperson

## Schlüsselbegriffe

CNCF, Monitoring, Logging, Serverless, Operator Pattern, Function as a Service, Autoscaler, RBAC, Helm, Horizontal Pod Autoscaler, Health Probe Pattern

## Lerninhalte

* Versteht die Funktion und Anwendung des Health Probe Patterns zur Überwachung und Gewährleistung der Betriebsbereitschaft von Anwendungen in Cloud-nativen Umgebungen und kann ein solches konfigurieren. 
* Kennt die Rolle und Einsatzmöglichkeiten von *Init Container* zur Initialisierung von Anwendungscontainern in Kubernetes.
* Versteht die Arbeitsweise und Konfiguration des Horizontal Pod Autoscalers für die automatische Skalierung von Anwendungsinstanzen basierend auf deren Leistung und Auslastung.
* Kennt die Prinzipien und Implementierung der Zugriffssteuerung mittels Role-Based Access Control (RBAC) für die sichere Verwaltung von Berechtigungen in Cloud-Umgebungen und kann diese konfigurieren. 
* Versteht die Bedeutung und Anwendung von Helm als Paketmanager für Kubernetes zur Vereinfachung der Bereitstellung und Verwaltung von Anwendungen
* Kennt die Wichtigsten Funktionen von Helm und kann diese praktisch Einsetzen.
* Kennt das Operator Pattern und dessen Bedeutung für die Automatisierung der Verwaltung und des Betriebs von Cloud-nativen Anwendungen.

## Übungen und Praxis

- **Health Probe Pattern**
  - Konfiguration einer Liveness Probe
  - Einrichtung einer Readiness Probe für eine Demo-Anwendung
- **Init Container**
  - Erstellung eines Pod mit einem Init Container, der Vorbedingungen prüft
  - Init Container zur Datenbankinitialisierung vor App-Start
- **Horizontal Pod Autoscaler**
  - Skalierungstest unter Last mit dem Horizontal Pod Autoscaler
  - Autoscaler-Konfiguration anhand CPU/Memory-Auslastung
- **Zugriffssteuerung (Role Based Access, RBAC)**
  - Erstellung von RBAC-Rollen und -Rollenbindungen
  - Definition und Anwendung benutzerdefinierter Rollen
- **Helm - der Paketmanager für Kubernetes**
  - Deployment einer Anwendung mit einem Helm-Chart
  - Erstellung eines eigenen Helm-Charts für eine einfache Anwendung
- **Operator Pattern**
  - Einsatz eines bestehenden Operators (z.B. Database Operator)
  - Grundlegende Implementierung eines eigenen Operators

## Lehr- und Lernformen

Vorträge, Lehrgespräche, Workshop, Einzel- und Gruppenarbeiten, Lernfragen, Präsentationen

## Lehrmittel

* Kubernetes Cluster
* CNCF Projects








