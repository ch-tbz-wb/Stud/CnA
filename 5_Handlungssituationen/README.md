# Handlungsziele und Handlungssituationen 

## Modul CNA1

### **1. Health Probe Pattern**

Der Studierende ist in der Lage, Health Probes (Liveness und Readiness Probes) in Kubernetes zu konfigurieren, um sicherzustellen, dass Anwendungen korrekt ausgeführt werden und bei Fehlern automatisch wiederhergestellt werden können.

**Typische Handlungssituation:**  
Ein Unternehmen betreibt eine Microservice-Anwendung in einem Kubernetes-Cluster. Einer der Microservices stürzt häufig ab, ohne dass dies vom Cluster bemerkt wird, was zu Ausfallzeiten führt. Der Studierende wird beauftragt, Liveness und Readiness Probes zu konfigurieren, um sicherzustellen, dass der Service bei Fehlern automatisch neu gestartet wird und nur gesunde Pods Anfragen erhalten.

---

### **2. Init Container**

Der Studierende kann Init Containers verwenden, um sicherzustellen, dass notwendige Voraussetzungen vor dem Start der Hauptanwendung in einem Kubernetes Pod erfüllt sind.

**Typische Handlungssituation:**  
Eine Anwendung benötigt vor ihrem Start Datenbankmigrationen und das Laden bestimmter Konfigurationen. Der Studierende wird beauftragt, einen Init Container zu erstellen, der diese Vorbereitungen durchführt, bevor der Hauptcontainer im Pod gestartet wird, um sicherzustellen, dass die Anwendung in einer vollständig vorbereiteten Umgebung ausgeführt wird.

---

### **3. Zugriffssteuerung (Role Based Access, RBAC)**

Der Studierende ist in der Lage, Role-Based Access Control (RBAC) in Kubernetes zu konfigurieren, um den Zugriff auf Ressourcen basierend auf spezifischen Rollen und Berechtigungen zu steuern.

**Typische Handlungssituation:**  
Ein Unternehmen stellt fest, dass alle Entwickler vollen Zugriff auf den Kubernetes-Cluster haben, was zu unautorisierten Änderungen und potenziellen Sicherheitsproblemen führt. Der Studierende wird beauftragt, ein RBAC-System einzurichten, bei dem nur bestimmte Entwickler auf bestimmte Ressourcen zugreifen können, basierend auf ihren Rollen (z.B. Lesezugriff, Schreibzugriff).

---

### **4. Helm - der Paketmanager für Kubernetes**

Der Studierende ist in der Lage, Helm-Charts zu erstellen und anzuwenden, um Kubernetes-Anwendungen effizient zu paketieren, zu versionieren und bereitzustellen.

**Typische Handlungssituation:**  
Ein Unternehmen muss mehrere Kubernetes-Anwendungen wiederholt in verschiedenen Umgebungen (Development, Staging, Production) bereitstellen. Der Studierende wird beauftragt, Helm zu verwenden, um diese Anwendungen in Helm-Charts zu paketieren, sodass sie einfach installiert, aktualisiert und verwaltet werden können, ohne dass jedes Mal alle Konfigurationsdateien manuell angepasst werden müssen.

---

### **5. Operator Pattern**

Der Studierende ist in der Lage, Kubernetes Operatoren zu entwickeln und zu implementieren, um die Automatisierung von komplexen Anwendungen und deren Lifecycle-Management im Kubernetes-Cluster zu ermöglichen.

**Typische Handlungssituation:**  
Ein Unternehmen betreibt eine hochkomplexe, zustandsbehaftete Anwendung, die regelmässige Wartungsaufgaben wie Backups und Skalierungen erfordert. Diese Aufgaben werden aktuell manuell durchgeführt, was zu Fehlern und Ineffizienz führt. Der Studierende wird beauftragt, einen Operator zu entwickeln, der diese Aufgaben automatisiert und sicherstellt, dass die Anwendung selbstständig skaliert, Backups durchführt und Wiederherstellungen nach Bedarf einleitet.

## Modul CNA2

Hier sind die Handlungsziele und typischen Handlungssituationen für die genannten Kubernetes-Themen:

### **6. Monitoring**

Der Studierende ist in der Lage, ein effektives Monitoring-System für Kubernetes-Cluster zu implementieren, das Ressourcen, Pod-Status und Anwendungsleistung überwacht und Alarme bei Problemen auslöst.

**Typische Handlungssituation:**  
In einem Unternehmen betreibt ein Team ein Kubernetes-Cluster, auf dem mehrere kritische Anwendungen laufen. Das Management möchte sicherstellen, dass die Ressourcen wie CPU, Arbeitsspeicher und Netzwerkauslastung kontinuierlich überwacht werden. Der Studierende wird beauftragt, ein Monitoring-System mit **Prometheus** und **Grafana** zu implementieren, das sowohl die Cluster-Ressourcen als auch die Performance der Anwendungen überwacht und Alarme auslöst, wenn Grenzwerte überschritten werden.

---

### **7. Logging (Logfiles sammeln, filtern und auswerten)**

Der Studierende kann ein zentrales Logging-System für Kubernetes implementieren, das Logfiles von verschiedenen Anwendungen und Pods sammelt, filtert und für die Analyse aufbereitet.

**Typische Handlungssituation:**  
Ein Unternehmen möchte Logs aus seinen Kubernetes-Anwendungen zentral erfassen und analysieren, um Fehler schnell identifizieren zu können. Der Studierende wird beauftragt, ein Logging-Tool wie **Fluentd** oder **ELK Stack (Elasticsearch, Logstash, Kibana)** zu implementieren, das Logfiles von allen Pods im Cluster sammelt, filtert und an einem zentralen Ort speichert. Zusätzlich soll er Filter konfigurieren, um nur relevante Logs für die Analyse zu speichern.

---

### **8. Service Mesh**

Der Studierende ist in der Lage, ein Service Mesh in Kubernetes zu implementieren, um Kommunikation zwischen Microservices zu verwalten, Traffic zu steuern und Sicherheit durch Verschlüsselung und Authentifizierung zu gewährleisten.

**Typische Handlungssituation:**  
In einer Kubernetes-Umgebung kommunizieren zahlreiche Microservices miteinander. Das Team steht vor Herausforderungen in Bezug auf die Sichtbarkeit des Netzwerks, Traffic-Management und Sicherheit. Der Studierende wird beauftragt, ein Service Mesh wie **Istio** oder **Linkerd** zu implementieren, um den Netzwerkverkehr zwischen den Microservices zu überwachen und zu steuern. Dabei soll er sicherstellen, dass die Kommunikation zwischen den Services sicher (z.B. durch mTLS) und effizient verläuft.

---

### **9. Serverless (Function as a Service, FaaS)**

Der Studierende kann eine Serverless-Architektur auf Kubernetes einrichten, die es ermöglicht, Funktionen nach Bedarf auszuführen, ohne ständig Ressourcen bereitzuhalten.

**Typische Handlungssituation:**  
Ein Unternehmen möchte eine Serverless-Plattform in seiner Kubernetes-Umgebung bereitstellen, um bestimmte Funktionen nur bei Bedarf auszuführen und so Ressourcen zu sparen. Der Studierende wird beauftragt, eine **FaaS-Lösung** wie **Kubernetes-based OpenFaaS** oder **Knative** zu implementieren, mit der Entwickler ihre Funktionen als Microservices bereitstellen können, die nur bei Anfragen ausgelöst werden und sich dynamisch skalieren.

---

### **10. Network Policies**

Der Studierende ist in der Lage, Network Policies in Kubernetes zu konfigurieren, um den Datenverkehr zwischen Pods und Services gezielt zu steuern und zu sichern.

**Typische Handlungssituation:**  
In einem Kubernetes-Cluster gibt es Bedenken hinsichtlich der Netzwerksicherheit, da alle Pods standardmässig miteinander kommunizieren können. Der Studierende wird beauftragt, Network Policies zu konfigurieren, die den Datenverkehr zwischen den Pods nur auf bestimmte, autorisierte Verbindungen beschränken. Dabei soll er sicherstellen, dass sensible Services nur von berechtigten Pods angesprochen werden können, um die Netzwerksicherheit zu gewährleisten.
