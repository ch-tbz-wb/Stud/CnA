## Operator Pattern

![](../x_gitressourcen/b-iiot.png)

- - -

Das Operator-Pattern ist angelehnt an einen menschlichen Operator (Bediener), der einen Dienst oder eine Reihe von Diensten verwaltet.

Mit dem Operator Pattern, kann man Aufgaben automatisieren und so die Grundfunktionalität von Kubernetes erweitern ohne Kubernetes selber ändern zu müssen.

Beispiele für Automatisierungen
* Bereitstellen einer Anwendung bei Bedarf
* Erstellen und Wiederherstellen von Sicherungen einer Anwendung
* Behandlung von Upgrades (CD) des Anwendungscodes sowie der damit verbundenen Änderungen wie Datenbankschemata oder zusätzliche Konfigurationseinstellungen
* Zur Verfügungstellung eines Dienstes für Anwendungen, die Kubernetes-APIs nicht unterstützen, z.B. damit diese Anwendungen neue Pods starten können.
* Simulation von Fehlern in Ihrem Cluster, um dessen Ausfallsicherheit zu testen

### Hands-on

* [IIoT Device Registry inkl. MQTTOperator](https://gitlab.com/ch-mc-b/autoshop-ms/infra/iiot)
* [Einfacher Operator](operator/)
* [Operator inkl. Custom Resources](custom/)

### Links

* [Operator Framework](https://operatorframework.io/)
* [Operator Hub](https://operatorhub.io/)
* [Python (Kopf) Operator](https://kopf.readthedocs.io/en/stable/)
* [Shell Operator](https://github.com/fl
ant/shell-operator)
* [https://entwickler.de/python/kubernetes-operatoren-python](Kubernetes-Operatoren mit Python: So geht's!)
