data "maas_vm_hosts" "default" {
  id = "default"
}

# VM Namen definieren
variable "control_planes" {
  type = list(string)
  default = ["cp1", "cp2", "cp3"]
}

# Erzeuge eine Liste von Maps mit Hostname und KVM-Nummer
locals {
  kvm_hosts = { for idx, name in data.maas_vm_hosts.default.name : name => data.maas_vm_hosts.default.no[idx] }
}

# Erstelle separate Ressourcen für jede VM auf jedem KVM-Host
resource "maas_vm_instance" "control_planes" {
  for_each = { for combo in setproduct(keys(local.kvm_hosts), var.control_planes) : "${combo[0]}-${combo[1]}" => combo }

  kvm_no    = local.kvm_hosts[each.value[0]]
  hostname  = "cna-${each.value[0]}-${each.value[1]}"
  cpu_count = 4
  memory    = 10240
  storage   = 64
  user_data = templatefile("cloud-init-${each.value[1]}.yaml", {})
  zone      = var.vpn
}
