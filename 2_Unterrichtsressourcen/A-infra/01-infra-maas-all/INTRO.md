01-Infrastruktur MAAS.io 
========

SSH-Zugriff
-----------

%{ for fqdn in hostnames ~}
    ssh -i ~/.ssh/lerncloud ubuntu@${fqdn}  
%{ endfor ~}

Shell in a Box
--------------

Als Alternative zu ssh steht eine Shell, via Browser, zur Verfügung.

%{ for fqdn in hostnames ~}
    https://${fqdn}:4200
%{ endfor ~}


Beispiele
---------

Die Umgebung beinhaltet eine Vielzahl von Beispielen als Juypter Notebooks. Die Jupyter Notebook Oberfläche ist wie folgt erreichbar:

%{ for fqdn in hostnames ~}
    http://${fqdn}:32188/tree/CnA/2_Unterrichtsressourcen/A-infra/README.ipynb 
%{ endfor ~}


