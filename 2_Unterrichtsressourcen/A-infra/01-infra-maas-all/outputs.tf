
locals {
  cp1_hostnames = [for vm in maas_vm_instance.control_planes : vm.hostname if length(regexall("cp1", vm.hostname)) > 0]
}

locals {
  rendered_intro = templatefile("${path.module}/INTRO.md", {
    hostnames = local.cp1_hostnames
  })
}

output "rendered_intro" {
  description = "Aggregierter Intro-Text für alle cp1-Hosts"
  value       = local.rendered_intro
}
