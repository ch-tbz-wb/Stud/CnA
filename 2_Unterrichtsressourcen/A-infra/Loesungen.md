### Lösungen

Wie Installiere ich den [cert-manager](cert-manager.md) auf `k3` und `k0s`

**Antwort**
* [Alternative Installation](https://cert-manager.io/docs/installation/kubectl/)   

---

Warum funktioniert [kube-virt](kube-virt.md) nicht auf `k0s`, was fehlt?

**Antwort**
* Storage Class Management
    
---

Warum funktioniert [kube-virt](kube-virt.md) Variante a) weder mit `microk8s` noch mit `k0s`?

**Antwort**
* microk8s legt seine Dateien unter `/var/lib/snap` ab und nicht in den Standardverzeichnissen von Kubernetes.

---

Mittels 

    kubectl apply -f https://raw.githubusercontent.com/mc-b/lerncloud/master/addons/dashboard.yaml
    
kann ich das Kubernetes Dashboard auch auf `k3s`und `k0s` installieren. Warum kann ich nichts anwählen?

**Antwort**
* k0s und k3s haben Standardmässig RBAC aktiviert, dem Dashboard UI fehlen die Rechte.

---