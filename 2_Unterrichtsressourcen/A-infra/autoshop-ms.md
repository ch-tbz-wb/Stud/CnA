## Auto Shop GmbH

Die Applikation [autoshop-ms](https://gitlab.com/ch-mc-b/autoshop-ms/app/shop) ist Mandanten fähig.

Die Auto Shop GmbH braucht für jedes Land in der D.A.CH Region eine Instanz. Diese Installieren wir mittels `helm`.

Zuerst ist in die erste erstelle VM zu wechseln (Output von terraform im Verzeichnis `01-infra-maas`).

    ssh -i ~/.ssh/lerncloud ubuntu@cna-cloud-hf-08-cp1.maas

Wir verwenden [autoshop-ms](https://gitlab.com/ch-mc-b/autoshop-ms/app/shop) und installieren es mittels `helm`

    export KVMHOST=cloud-hf-08
    git clone https://gitlab.com/ch-mc-b/autoshop-ms/infra/helm.git
    cd helm
    helm install autoshop-ch ./autoshop --namespace m01-ch --create-namespace --set ingress.host=ch.${KVMHOST}-edutbz.com --set image.tag=2.1.0
    helm install autoshop-at ./autoshop --namespace m01-at --create-namespace --set ingress.host=at.${KVMHOST}-edutbz.com --set image.tag=2.0.0 
    helm install autoshop-d ./autoshop  --namespace m01-d  --create-namespace --set ingress.host=d.${KVMHOST}-edutbz.com  --set image.tag=1.0.0 
    
**Hinweis**: es werden die Zertifikate von Kubernetes verwendet. Testen mittels Anwahl des URL welcher helm ausgibt. 

**ACHTUNG**: `cloud-hf-08` ist durch euren KVM-Host zu ersetzen für AWS sind die entsprechenden DNS Einträge (`sudo cloud-init query ds.meta_data.public-hostname`) von AWS zu verwenden. Hier und in allen weiteren Deklarationen!

Kontrolle ob alle Ressourcen sauber erstellt wurden:

    kubectl get all,ingress -n m01-ch
    
Mittels `curl` und `-H "Host:"` können wir gezielt einen URL ansprechen

    export KVMHOST=cloud-hf-08
    curl -k -H "Host: ch.${KVMHOST}-edutbz.com" https://localhost:443/webshop   
    
**Fehleranalyse läuft ein Ingress Dienst?**

Dann sollten auch mindestens eine ingressClass vorhanden sein.     

    kubectl get ingressClasses
