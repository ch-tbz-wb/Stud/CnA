## Cert-Manager

Den Cert-Manager gibt es als [microk8s Addon](), deshalb ist die Installation einfach:

    microk8s enable cert-manager
    
* [Alternative Installation](https://cert-manager.io/docs/installation/kubectl/)

#### Self-Signed Issuer

**CA für interne Zertifikate einrichten**

Da es sich um interne DNS-Domains handelt, kann Let's Encrypt nicht genutzt werden. 

Stattdessen verwenden wir Self-Signed Issuer (einfach). Das erstellt ein selbstsigniertes Zertifikat für interne Zwecke.

    kubectl apply -f - <<EOF
    apiVersion: cert-manager.io/v1
    kind: ClusterIssuer
    metadata:
      name: selfsigned-cluster-issuer
    spec:
      selfSigned: {}
    EOF
    
**Zertifikatsanfrage für die interne Domain erstellen**

Wir erstellen pro Kubernetes Namespace ein Zertifikat

    kubectl apply -f - <<EOF
    apiVersion: cert-manager.io/v1
    kind: Certificate
    metadata:
      name: selfsigned-cert-ch
      namespace: m01-ch
    spec:
      secretName: selfsigned-cert-secret
      duration: 24h
      renewBefore: 12h
      issuerRef:
        name: selfsigned-cluster-issuer
        kind: ClusterIssuer
      commonName: ch.cloud-hf-08-edutbz.com
      dnsNames:
        - ch.cloud-hf-08-edutbz.com
    EOF
    
Die Befehle sind für die Namespaces `m01-at` und `m01-d` zu wiederholen. Anpassen Namespace, commonName und dnsNames nicht vergessen!
    
**Verwendung des Zertifikats**

Das Zertifikat liegt nun als Kubernetes-Secret unter internal-cert-secret und kann z. B. für Ingress oder TLS-Services genutzt werden   

    kubectl apply -f - <<EOF
    apiVersion: networking.k8s.io/v1
    kind: Ingress
    metadata:
      name: webshop 
      namespace: m01-ch
      annotations:
        cert-manager.io/issuer: selfsigned-cluster-issuer
    spec:
      rules:
      - host: ch.cloud-hf-08-edutbz.com
        http:
          paths:
          - path: /webshop
            pathType: Prefix
            backend:
              service:
                name: webshop
                port:
                  number: 8080
      tls:
      - hosts:
        - ch.cloud-hf-08-edutbz.com
        secretName: selfsigned-cert-secret
    EOF
    
Testen ob das Zertifikat übernommen wurde

    helm status autoshop-ch --namespace m01-ch
    
URL anwählen und Zertifikatsdetails im Browser anschauen. Neu steht "Allgemeiner Name (CN)  ch.cloud-hf-08-edutbz.com"        

Die Befehle sind für die Namespaces `m01-at` und `m01-d` zu wiederholen. Anpassen Namespace, host und hosts nicht vergessen!

**Auftrag**

Verlängert die Gültigkeitsdauer des Zertifikates, damit es länger als 24h gültig ist.

**Kontrolle**

Auf jeder Node läuft ein `nginx` welcher die Ingress (Reverse Proxy) Funktionalität zur Verfügung stellt. Der `nginx` verwendet `location` Einträge für Reverse Proxy, diese können wie folgt angeschaut werden:

    kubectl exec daemonset/nginx-ingress-microk8s-controller -n ingress -- cat /etc/nginx/nginx.conf | grep location

**Am Schluss sollte jeder Namespace ein anderes Zertifikat verwenden.**

#### Mit Let's Encrypt

Funktioniert nur mit öffentlichem Server. Port 80 und 443 müssen für Let's Encrypt erreichbar sein.

    Cert-manager is installed. As a next step, try creating an Issuer
    for Let's Encrypt by creating the following resource:
    
    $ microk8s kubectl apply -f - <<EOF
    ---
    apiVersion: cert-manager.io/v1
    kind: Issuer
    metadata:
      name: letsencrypt
    spec:
      acme:
        # You must replace this email address with your own.
        # Let's Encrypt will use this to contact you about expiring
        # certificates, and issues related to your account.
        email: me@example.com
        server: https://acme-v02.api.letsencrypt.org/directory
        privateKeySecretRef:
          # Secret resource that will be used to store the account's private key.
          name: letsencrypt-account-key
        # Add a single challenge solver, HTTP01 using nginx
        solvers:
        - http01:
            ingress:
              ingressClassName: nginx
    EOF
    
    Then, you can create an ingress to expose 'my-service:80' on 'https://my-service.example.com' with:
    
    $ microk8s enable ingress
    $ microk8s kubectl create ingress my-ingress \
        --annotation cert-manager.io/issuer=letsencrypt \
        --rule 'my-service.example.com/*=my-service:80,tls=my-service-tls'
        
**Hinweise**:

Konfiguration wie immer über eine YAML-Datei. Für Let's Encrypt verwendet man ACME mit HTTP01. Dafür muss dann aber der Ingress via Port 80 vom Internet her erreichbar sein, d.h. es reicht nicht nur 443. Cert-Manager prüft zwischendurch, ob die hinterlegten Zertifikate/Secrets erneuert werden müssen und konfiguriert ganz kurz den Ingress um auf sich selber für die HTTP01-Validierung und nachher wieder zurück. 

Alternative Option: Falls von aussen her kein Port offen sein soll und trotzdem ein offizielles Zertifikat benötigt wird, kannst du es auch lösen via DNS01 und einer eigenen Domäne bei einem Anbieter mit API (z.B. HostTech mit dem 35Fr/Jahr DNS-Abo). 

Bei ACME (Let's Encrypt) ist zu beachten, dass es gewisse Limits gibt. Zum Testen kann aber ein Staging-Server unendlich oft verwendet werden, sind dann aber keine offiziellen Zertifikate. Dies ist evtl. auch ein Risiko, wenn du mit echten Zertifikaten herumhantierst, kommst du ans Limit und bist mit der Domain für eine Woche gesperrt. 


          
