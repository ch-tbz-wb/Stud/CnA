## KubeVirt

### Installation

    export VERSION=$(curl https://storage.googleapis.com/kubevirt-prow/release/kubevirt/kubevirt/stable.txt)
    echo $VERSION
    
    kubectl apply -f https://github.com/kubevirt/kubevirt/releases/download/${VERSION}/kubevirt-operator.yaml    
    kubectl apply -f https://github.com/kubevirt/kubevirt/releases/download/${VERSION}/kubevirt-cr.yaml
    
Emulation aktivieren, weil VM in VM. Bei Fehler solange ausführen bis kein Fehler mehr angezeigt wird:
    
    kubectl -n kubevirt patch kubevirt kubevirt --type=merge --patch '{"spec":{"configuration":{"developerConfiguration":{"useEmulation":true}}}}' 
    
Zustätzlich brauchen wir das `virtctl` Tools um uns z.B. mit der VMs zu verbinden.       
    
    wget https://github.com/kubevirt/kubevirt/releases/download/${VERSION}/virtctl-${VERSION}-linux-amd64
    chmod +x virtctl-${VERSION}-linux-amd64
    sudo mv virtctl-${VERSION}-linux-amd64 /usr/local/bin/virtctl 
    
Kontrollieren ob 6 Pods gestartet wurden:

    kubectl --namespace kubevirt get pods  
    
Wenn weniger als 6 Pods gestartet werden, fehlt dem Kubernetes-Node das entsprechende Label (z.B. bei microk8s oder k0s), das ihn als control-plane oder master kennzeichnet. Diese Labels können wie folgt gesetzt werden:

    kubectl label nodes $(kubectl get nodes -o custom-columns=NAME:.metadata.name | awk 'NR==2') node-role.kubernetes.io/master=
    kubectl label nodes $(kubectl get nodes -o custom-columns=NAME:.metadata.name | awk 'NR==2') node-role.kubernetes.io/control-plane=      
    
* [Quick Starts](https://kubevirt.io/user-guide/quickstarts/)
    
### Variante a) VM ab Container-Image

Warten bis 6 Container im K8s Namespace `kubevirt` gestartet sind:

    kubectl get pods -n kubevirt
    
VM starten und mit der Console verbinden:    

    kubectl apply -f https://kubevirt.io/labs/manifests/vm.yaml
    virtctl start testvm
    virtctl console testvm
    
Verlassen der Console mittels `Ctrl+]`.

### Variante b) VM mittels Containerized Data Importer

Warten bis 6 Container (bei drei Nodes 9 Container) im K8s Namespace `kubevirt` gestartet sind:

    kubectl get pods -n kubevirt
    
Installation [Containerized Data Importer](https://kubevirt.io/user-guide/storage/containerized_data_importer/)

    export VERSION=$(curl -Ls https://github.com/kubevirt/containerized-data-importer/releases/latest | grep -m 1 -o "v[0-9]\.[0-9]*\.[0-9]*")
    echo $VERSION 
    
    kubectl apply -f https://github.com/kubevirt/containerized-data-importer/releases/download/$VERSION/cdi-operator.yaml
    kubectl apply -f https://github.com/kubevirt/containerized-data-importer/releases/download/$VERSION/cdi-cr.yaml

* [Containerized Data Importer](https://kubevirt.io/user-guide/storage/containerized_data_importer/)

Erstellen des Data Volumes mit dem Cloud-Image von Debian

    kubectl apply -f - <<EOF
    apiVersion: cdi.kubevirt.io/v1beta1
    kind: DataVolume
    metadata:
      name: "debian"
    spec:
      source:
        http:
          url: "https://cloud.debian.org/images/cloud/bullseye/latest/debian-11-generic-amd64.qcow2"
      pvc:
        accessModes:
        - ReadWriteOnce
        resources:
          requests:
            storage: "4Gi"         
    EOF
    
VM erstellen

    kubectl apply -f - <<EOF
    apiVersion: kubevirt.io/v1
    kind: VirtualMachine
    metadata:
      labels:
        kubevirt.io/os: linux
      name: debian
    spec:
      running: true
      template:
        metadata:
          creationTimestamp: null
          labels:
            kubevirt.io/domain: debian
        spec:
          domain:
            cpu:
              cores: 1
            devices:
              disks:
              - disk:
                  bus: virtio
                name: disk0
              - cdrom:
                  bus: sata
                  readonly: true
                name: cloudinitdisk
            resources:
              requests:
                memory: 2G
          volumes:
          - name: disk0
            persistentVolumeClaim:
              claimName: debian
          - cloudInitNoCloud:
              userData: |
                #cloud-config
                system_info:
                  default_user:
                    name: debian
                    home: /home/debian
                password: insecure
                chpasswd: { expire: False }
                hostname: debian-k8s
                ssh_pwauth: True
                disable_root: false
                ssh_authorized_keys:
                - ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDUHol1mBvP5Nwe3Bzbpq4GsHTSw96phXLZ27aPiRdrzhnQ2jMu4kSgv9xFsnpZgBsQa84EhdJQMZz8EOeuhvYuJtmhAVzAvNjjRak+bpxLPdWlox1pLJTuhcIqfTTSfBYJYB68VRAXJ29ocQB7qn7aDj6Cuw3s9IyXoaKhyb4n7I8yI3r0U30NAcMjyvV3LYOXx/JQbX+PjVsJMzp2NlrC7snz8gcSKxUtL/eF0g+WnC75iuhBbKbNPr7QP/ItHaAh9Tv5a3myBLNZQ56SgnSCgmS0EUVeMNsO8XaaKr2H2x5592IIoz7YRyL4wlOmj35bQocwdahdOCFI7nT9fr6f insecure@lerncloud
                packages:
                  - qemu-guest-agent               
            name: cloudinitdisk
    EOF
    
Kontrollieren ob alles Eingerichtet wurde:

    kubectl get sc,pv,pvc,dv,vm,vmi -o wide    
    
### Aufräumen

    kubectl delete vm debian
    kubectl delete datavolume debian
    
    export VERSION=$(curl -Ls https://github.com/kubevirt/containerized-data-importer/releases/latest | grep -m 1 -o "v[0-9]\.[0-9]*\.[0-9]*")
    kubectl delete -f https://github.com/kubevirt/containerized-data-importer/releases/download/$VERSION/cdi-operator.yaml
    kubectl delete -f https://github.com/kubevirt/containerized-data-importer/releases/download/$VERSION/cdi-cr.yaml    
    
    export VERSION=$(curl https://storage.googleapis.com/kubevirt-prow/release/kubevirt/kubevirt/stable.txt)
    kubectl delete -f https://github.com/kubevirt/kubevirt/releases/download/${VERSION}/kubevirt-operator.yaml    
    kubectl delete -f https://github.com/kubevirt/kubevirt/releases/download/${VERSION}/kubevirt-cr.yaml    
    
### Links

* [KubeVirt User Guide](https://kubevirt.io/user-guide/cluster_admin/installation/)
* [KubeVirt Part 1 - Run VMs like a Pod](https://eng.d2iq.com/blog/kubevirt-part-1-run-vms-like-a-pod/)
* [panic: timed out waiting for domain to be defined](https://github.com/kubevirt/kubevirt/issues/4556)
* [Failed to connect socket to '/var/run/libvirt/libvirt-sock](https://github.com/kubevirt/kubevirt/issues/5069)
* [kind](https://kubevirt.io/quickstart_kind/)

* [How to Set Up Hyper-V Nested Virtualization](https://adamtheautomator.com/nested-virtualization/#Enabling_Nested_Virtualization)
* [How to Install KVM on Ubuntu 20.04](https://phoenixnap.com/kb/ubuntu-install-kvm)

* [Kubernetes-Leistung für virtuelle Maschinen mit KubeVirt - Civo.com](https://www.civo.com/learn/kubernetes-power-for-virtual-machines-using-kubevirt)

* [Ubuntu Installations Images](https://cloud-images.ubuntu.com/)
    

    