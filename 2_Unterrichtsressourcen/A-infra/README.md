## Infrastruktur

Unser Kunde, die **Auto Shop GmbH**, ist in der **DACH-Region** tätig. Der Shop wird derzeit über [Docker Compose](https://gitlab.com/ch-mc-b/autoshop-ms/app/shop) betrieben, soll jedoch auf **Kubernetes** migriert werden.  

Da die **IT-Abteilung** unsicher ist, welche **Kubernetes-Distribution** und welche Kombination von **CNCF.io-Projekten** die beste Wahl für das Unternehmen ist, haben die Engineers beschlossen, die Infrastruktur in separate **Terraform-Projekte** zu unterteilen:  

- **Infrastruktur (VMs)** – `01-xxx`  
- **DNS-Einträge** – `01-xxx-dns`  
- **Kubernetes-Distributionen** – `02-xxx`  

Diese Aufteilung ermöglicht es, verschiedene Konfigurationen zu testen und die optimale Lösung zu finden.  

### Grundsätzliche Anforderungen:  
- **Länderspezifische DNS-Einträge**: Für jedes Land in der DACH-Region wird ein eigener **DNS-Eintrag** bereitgestellt.  
- **Namespace-Isolation**: Die verschiedenen Länder werden in separaten **Kubernetes-Namespaces** betrieben.  

### Struktur

* [01-infra](01-infra) - Grundstruktur 1 - x VMs mit minimaler Software
* [02-microk8s](02-microk8s) - Ubuntu microk8s Kubernetes Distribution (empfohlen) 
* [02-k3s](02-k3s) - Rancher (jetzt SUSE) k3s Kubernetes Distribution
* [02-k0s](02-k0s) - Mirantis K0s Kubernetes Distribution

Alle K8s Distributionen werden mittels Terraform installiert. `terraform destroy` löscht die K8s Distribution wieder. So müssen nicht immer alle VMs gelöscht und neu angelegt werden.

### Vorgehen

Setzen der benötigten Umgebungsvariablen:

* **TF_VAR_vpn** - VPN, Standard = `default`
* **TF_VAR_url** - URL MAAS, z.B. `http://10.0.24.8:5240/MAAS`
* **TF_VAR_key** - API Key MAAS

Erstellen der Infrastruktur bzw. Installation des entsprechenden CNCF.io Projektes.

    cd [Verzeichnis]
    terraform workspace new [KVM-Host]
    terraform init
    terraform apply -auto-approve
    
**Hinweis**: Der Terraform Workspace wird verwendet um auf den richtigen KVM-Host zuzusteuern, deshalb pro Verzeichnis **immer** immer zuerst der Terraform Workspace zu ändern bzw. anzulegen (`terraform workspace new [KVM-Host]`).

Beim Erstellen der VMs werden die IP-Adresse und FQDN als Outputs im Terraform State gespeichert.

Die nachfolgenden Terraform Deklarationen verwenden diese.

Beispiel 01-xxxx `output.tf`

    output "fqdn_control-plane-1" {
      value       = module.control-plane-1.fqdn_vm
      description = "The FQDN of the server instance."
    }
    
Wiederverwendung, von `fqdn_control-plane-1` in 02-xxxx `data.tf`

    data "terraform_remote_state" "infra" {
      backend = "local"
    
      config = {
        path = "../01-infra-maas/terraform.tfstate.d/${terraform.workspace}/terraform.tfstate"
      }
    }    

und `main.tf`

    resource "null_resource" "run_script_on_cp-1" {
    
      triggers = {
        fqdn = data.terraform_remote_state.infra.outputs.fqdn_control-plane-1
      }    

### DNS DACH-Region Einträge (nur MAAS Umgebung!)

Wechselt ins Verzeichnis `01-infra-maas-dns` und führt die obigen terraform Befehle aus.

Es werden DNS Einträge für die DACH-Region erstellt

* ch.[KVM-Host]-edutbz.com
* at.[KVM-Host]-edutbz.com
* d.[KVM-Host]-edutbz.com

Details [siehe](01-infra-maas-dns)

**Tipp**: wenn die DNS Einträge mittels OpenVPN Verbindung nicht aufgelöst werden, schreibt die DNS Namen in `/etc/hosts` bzw. `C:\Windows\System32\Drivers\etc\hosts`

### Kubernetes

#### 1. Teil (MAAS)

Installiert für den ersten Teil die `02-microk8s` Kubernetes Distribution.

Wechselt dazu in ein `02-microk8s` Verzeichnis und führt die obigen terraform Befehle aus.

**Installation der Applikationen und Zusatzprodukte**

* [Applikationen](autoshop-ms.md)
* [Zertifikate (cert-manager)](cert-manager.md)
* [Container Image Registry (Harbor)](harbor.md)
* [VM für Datenbank (kube-virt)](kube-virt.md)
* [Persistente Ablage (longhorn](longhorn.md)
* [LoadBalancer MetalLB](metallb.md)

#### 2. Teil (AWS)

Für den zweiten Teil verwenden wir die `01-infra-aws` Umgebung. 

Diese Installiert auf der ersten VM `microk8s`, auf der zweiten `k0s` und auf der dritten `k3s`.

Wechselt dazu in den Terraform Workspace `aws` bzw. erstellt diesen:

    cd 01-infra-aws
    terraform workspace new aws
    terraform init
    terraform apply -auto-approve

Beachtet die Meldungen von `terraform apply`. Diese Meldungen kann man nachträglich mittels `terraform show` wieder anzeigen.

Es wird das Terraform Modul [git::https://github.com/mc-b/terraform-lerncloud-aws](https://github.com/mc-b/terraform-lerncloud-aws) verwendet. Diese legt neben den VMs auch Security-Groups an. Standardmässig sind die Standardports 22, 80, 443 gegen das Internet offen. Alle anderen Ports sind nur für die ausgehende IP offen, d.h. erstellt er die VMs in der TBZ, müsst ihr in der Firma oder Zuhause die Security-Groups anpassen.

Mittels 

    kubectl apply -f https://raw.githubusercontent.com/mc-b/lerncloud/master/addons/dashboard.yaml
    
kann man das Kubernetes Dashboard auch auf den `k3s`und `k0s` VMs installieren. 
    
Die Adresse und den Port erhält man mittels

    echo "https://$(cat ~/work/server-ip)":8443   
    
Aber, weil RBAC aktiviert ist, kann nichts angewählt werden.     

Das Dashboard kann, durch hinzufügen des ServicesAccounts vom Dashboard zum cluster-admin, freigeschaltet werden:

    kubectl apply -f - <<EOF
    apiVersion: rbac.authorization.k8s.io/v1
    kind: ClusterRoleBinding
    metadata:
      name: kubernetes-dashboard-admin
    subjects:
    - kind: ServiceAccount
      name: kubernetes-dashboard
      namespace: kubernetes-dashboard
    roleRef:
      kind: ClusterRole
      name: cluster-admin
      apiGroup: rbac.authorization.k8s.io
    EOF
    
**Hinweis**: `pstree -npTA` liefert ein hierarchische Anordnung der Prozesse. Diese eignet sich gut für einen Vergleich der K8s Distributionen.

**Installation der Applikationen und Zusatzprodukte**

* [Applikationen](autoshop-ms.md)
* [Zertifikate (cert-manager)](cert-manager.md)
* [Container Image Registry (Harbor)](harbor.md)
* [Persistente Ablage (longhorn](longhorn.md)
* [VM für Datenbank (kube-virt)](kube-virt.md)
