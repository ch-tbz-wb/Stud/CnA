###
#   Outputs DNS Names

output "intro" {
  value = templatefile("INTRO.md", {
    fqdn1 = maas_dns_record.control-plane-ch.fqdn
    fqdn2 = maas_dns_record.control-plane-at.fqdn
    fqdn3 = maas_dns_record.control-plane-d.fqdn
    }
  )
}