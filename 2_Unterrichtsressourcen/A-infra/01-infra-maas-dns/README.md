### 01 Infrastruktur - DNS Einträge

Erstellt die DNS Einträge für die DACH Region.

**ACHTUNG**: die Einträge können nicht via terraform gelöscht werden. Evtl. muss ein Terraform Reset durchgeführt werden

    rm -rf terra*
    terraform workspace new [KVM-Host]
    terraform init
    terraform apply -auto-approve
    

    
