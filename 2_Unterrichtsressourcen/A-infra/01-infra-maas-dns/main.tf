############
## DNS


# Terraform Workspace = Domain
resource "maas_dns_domain" "mydomain" {
  # Variante mit vollen Hostname, z.B. bei AWS
  # name          = "${data.terraform_remote_state.infra.outputs.fqdn_control-plane-1}-edutbz.com"
  name          = "${terraform.workspace}-edutbz.com"  
  authoritative = true

  # Stellt sicher, dass die Domain erst gelöscht wird, wenn die DNS-Einträge entfernt wurden
  lifecycle {
    prevent_destroy = false
  }
}

# DACH (Deutschland, Oesterreich, Schweiz) Einträge

# Deutschland
resource "maas_dns_record" "control-plane-d" {
  type = "A/AAAA"
  data = tolist(data.terraform_remote_state.infra.outputs.ip_control-plane-1)[0]
  fqdn = "d.${maas_dns_domain.mydomain.name}"
}

# Oesterreich
resource "maas_dns_record" "control-plane-at" {
  type = "A/AAAA"
  data = tolist(data.terraform_remote_state.infra.outputs.ip_control-plane-1)[0]
  fqdn = "at.${maas_dns_domain.mydomain.name}"
}

# Schweiz
resource "maas_dns_record" "control-plane-ch" {
  type = "A/AAAA"
  data = tolist(data.terraform_remote_state.infra.outputs.ip_control-plane-1)[0]
  fqdn = "ch.${maas_dns_domain.mydomain.name}"
}
