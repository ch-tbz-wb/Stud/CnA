01-infra-maas-dns
==========

Umgebung zum Modul: [CNA - Cloud-native Advanced](https://gitlab.com/ch-tbz-wb/Stud/CnA).

Web Shops
---------

    https://${fqdn1}/webshop
    https://${fqdn2}/webshop
    https://${fqdn3}/webshop