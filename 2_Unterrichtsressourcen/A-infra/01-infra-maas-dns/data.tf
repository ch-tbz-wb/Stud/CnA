# Import State von 01-infra

data "terraform_remote_state" "infra" {
  backend = "local"

  config = {
    path = "../01-infra-maas/terraform.tfstate.d/${terraform.workspace}/terraform.tfstate"
    #path = "D:/OneDrive/OneDrive - TBZ/TBZ/HF/terraform.state.d/cna/${terraform.workspace}/terraform.tfstate"
  }
}
