## MetalLB

MetalLB ist eine Load-Balancer-Implementierung für Bare-Metal -Kubernetes- Cluster, die Standard-Routingprotokolle verwendet.


MetalLB kann wie folgt in microk8s aktiviert werden:


    microk8s enable "metallb:10.0.24.XXX-10.0.24.XXX"
    
XXX ist durch den eigenen IP-Range (siehe Liste im MS Admin Teams) zu ersetzen.   

Anzeige der verwendeten IP-Adressen:

    kubectl get services -A | grep LoadBalancer 
    
Ausgabe der metalLB Konfiguration

    kubectl describe ipaddresspools -n metallb-system  