###
#   Installation, Deinstallation k0s mittels provisioner

resource "null_resource" "run_script_on_vms" {

  triggers = {
    fqdn = data.terraform_remote_state.infra.outputs.fqdn_control-plane-2
  }

  # SSH-Verbindung zur VM 
  connection {
    type        = "ssh"
    user        = "ubuntu"                 # Benutzername ist "ubuntu"
    private_key = file("~/.ssh/lerncloud") # Verwende den SSH-Schlüssel aus "~/.ssh/lerncloud"
    host        = self.triggers.fqdn       # VM-Name mit Suffix ".maas"
  }

  # Remote-Exec-Provisioner zum Ausführen des Shell-Skripts
  provisioner "remote-exec" {
    inline = [
      "curl -sSLf https://get.k0s.sh | sudo sh",
      "sudo k0s install controller --single",
      "sudo k0s start",
      "mkdir -p ~/.kube",
      "while [ ! -f /var/lib/k0s/pki/admin.conf ]; do sleep 5; done; sudo cp /var/lib/k0s/pki/admin.conf ~/.kube/config",
      "sudo chown $(id -u):$(id -g) ~/.kube/config",
      "sudo snap install helm --classic"
    ]
  }

  provisioner "remote-exec" {
    when = destroy
    inline = [
      "sudo k0s stop",
      "sudo k0s reset",
      "sudo rm -rf /var/lib/k0s /etc/k0s",
      "sudo rm -f /usr/local/bin/k0s"
    ]
    on_failure = continue
  }
}

