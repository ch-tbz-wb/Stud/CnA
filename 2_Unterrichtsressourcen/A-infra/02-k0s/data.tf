# Import State von 01-infra

data "terraform_remote_state" "infra" {
  backend = "local"

  config = {
    path = "../01-infra-aws/terraform.tfstate.d/${terraform.workspace}/terraform.tfstate"
  }
}
