###
#   Outputs wie IP-Adresse und DNS Name

output "ip_control-plane" {
  value = data.terraform_remote_state.infra.outputs.ip_control-plane-2
}

output "fqdn_control-plane" {
  value       = data.terraform_remote_state.infra.outputs.fqdn_control-plane-2
  description = "The FQDN of the server instance."
}

output "intro" {
  value = templatefile("INTRO.md", {
    ip   = element(tolist(data.terraform_remote_state.infra.outputs.ip_control-plane-2), 0)
    fqdn = data.terraform_remote_state.infra.outputs.fqdn_control-plane-2
    }
  )
}