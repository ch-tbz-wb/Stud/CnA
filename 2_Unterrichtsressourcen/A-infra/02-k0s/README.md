## k0s Kubernetes

k0s ist eine leichtgewichtige, benutzerfreundliche Kubernetes-Distribution, die entwickelt wurde, um Kubernetes einfach bereitzustellen und zu verwalten. Sie ist besonders geeignet für einfache Single-Node-Cluster, skalierbare Multi-Node-Umgebungen und Cloud-Edge-Deployments.

### k0s installieren

Nach dem Ausführen der `terraform` Befehle im Verzeichnis [01-infra](../01-infra)

    terraform workspace new [KVM-Host]
    terraform init
    terraform apply -auto-approve
    
### k0s deinstallieren

    terraform destroy -auto-approve

    