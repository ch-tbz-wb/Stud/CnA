## Longhorn.io

Longhorn ist ein leichtgewichtiges, zuverlässiges und leistungsstarkes verteiltes Blockspeichersystem für Kubernetes. Es wurde entwickelt, um Cloud-native, persistente Speicherlösungen bereitzustellen, die einfach zu installieren und zu verwalten sind. 

**Hauptmerkmale von Longhorn:**

- **Hohe Verfügbarkeit:** Durch die synchrone Replikation von Daten über mehrere Knoten hinweg stellt Longhorn sicher, dass keine einzelnen Fehlerquellen den Betrieb beeinträchtigen. 

- **Inkrementelle Snapshots und Backups:** Longhorn ermöglicht die Erstellung von inkrementellen Snapshots und Backups, wodurch der Speicher effizient genutzt und die Datensicherung vereinfacht wird. 

- **Einfache Installation und Verwaltung:** Die Bereitstellung von Longhorn in einem bestehenden Kubernetes-Cluster kann mit einem einzigen `kubectl apply`-Befehl oder über Helm-Charts erfolgen. Nach der Installation fügt Longhorn dem Cluster Unterstützung für persistente Volumes hinzu. 

- **Intuitive Benutzeroberfläche:** Ein benutzerfreundliches Dashboard erleichtert die Verwaltung und Überwachung des Speichersystems. 

**Voraussetzungen**

1. **iSCSI installieren und aktivieren**: Longhorn benötigt iSCSI für die Bereitstellung von Volumes.

       sudo apt-get install open-iscsi
       sudo modprobe iscsi_tcp
       lsmod | grep iscsi_tcp

2. **Multipath-Dienst deaktivieren**: Falls der `multipathd`-Dienst aktiv ist, sollte er deaktiviert werden, da er Konflikte mit Longhorn verursachen kann.

       sudo systemctl stop multipathd
       sudo systemctl disable multipathd
       sudo systemctl mask multipathd
   
**ACHTUNG**: diese Arbeiten sind auf jeder VM durchzuführen.          

3. **Kubernetes-Labels für den Master-Knoten setzen** (nur microk8s): Dies stellt sicher, dass der Master-Knoten korrekt identifiziert wird.

       kubectl label nodes $(kubectl get nodes -o custom-columns=NAME:.metadata.name | awk 'NR==2') node-role.kubernetes.io/master=
       kubectl label nodes $(kubectl get nodes -o custom-columns=NAME:.metadata.name | awk 'NR==2') node-role.kubernetes.io/control-plane=


**Umgebungsprüfung**

Bevor ihr mit der Installation fortfahrt, ist es ratsam, die Umgebung auf Kompatibilität zu prüfen:

    curl -sSfL https://raw.githubusercontent.com/longhorn/longhorn/v1.8.0/scripts/environment_check.sh | bash

### Installation von Longhorn

Die Installation erfolgt mittels `helm`, da so wichtige Parameter überschrieben werden können.

**Helm-Repository hinzufügen und aktualisieren**:

    helm repo add longhorn https://charts.longhorn.io

**Longhorn installieren**:

    helm install longhorn longhorn/longhorn \
     --namespace longhorn-system --create-namespace \
     --set defaultSettings.kubernetesClusterAutodetectionMethod="custom" \
     --set defaultSettings.kubeletRootDir="/var/snap/microk8s/common/var/lib/kubelet" \
     --set csi.kubeletRootDir="/var/snap/microk8s/common/var/lib/kubelet"

**Hinweis**: Die Angabe des `kubeletRootDir` ist speziell für `microk8s` erforderlich, da der Kubelet-Standardpfad hier abweicht.

### Testen der Installation

Um die erfolgreiche Installation zu überprüfen, können zwei PersistentVolumeClaims (PVCs) erstellt werden:

    kubectl apply -f - <<EOF
    apiVersion: v1
    kind: PersistentVolumeClaim
    metadata:
      name: longhorn-rwo
    spec:
      accessModes:
        - ReadWriteOnce
      storageClassName: longhorn
      resources:
        requests:
          storage: 2Gi
    ---
    apiVersion: v1
    kind: PersistentVolumeClaim
    metadata:
      name: longhorn-rwx 
    spec:
      accessModes:
        - ReadWriteMany
      storageClassName: longhorn
      resources:
        requests:
          storage: 2Gi
    EOF

Nach der Erstellung der PVCs könnt ihr deren Status mit folgendem Befehl überprüfen:

    kubectl get pvc
    
Wenn die PVCs den Status `Bound` haben, war die Installation erfolgreich.

Longhorn legt dabei automatisch PersistenVolumes an, welche mit der jeweiligen StorageClass verbunden sind.

    kubectl get sc,pv,pvc
    
### Verwenden des Speichers

Dazu erstellen wir zwei Pod, der eine schreibt Daten der andere liest diese.

    kubectl apply -f - <<EOF
    apiVersion: v1
    kind: Pod
    metadata:
      labels:
        app.kubernetes.io/name: web
      name: web
    spec:
      containers:
      - image: registry.gitlab.com/mc-b/misegr/httpd
        name: apache
        # Volumes im Container
        volumeMounts:
        - mountPath: "/usr/local/apache2/htdocs"
          name: "web-storage"    
        # Volumes im Container
        volumeMounts:
        - mountPath: "/usr/local/apache2/htdocs"
          name: "web-storage"
      # Volumes in Host      
      volumes:
        - name: web-storage
          persistentVolumeClaim:
            claimName: longhorn-rwx  
    ---            
    apiVersion: v1
    kind: Pod
    metadata:
      labels:
        app.kubernetes.io/name: puller
      name: puller
    spec:
      containers:
      - image: registry.gitlab.com/mc-b/misegr/debian:jessie
        name: file-puller
        # Just spin & wait forever
        command: [ "/bin/bash", "-c", "--" ]
        args: [ "while true; do echo \"<html><body><h1>Hallo es ist $(date)</h1></body></html>\" >/usr/local/apache2/htdocs/index.html; sleep 30; done;" ]    
        # Volumes im Container
        volumeMounts:
        - mountPath: "/usr/local/apache2/htdocs"
          name: "web-storage"
      # Volumes in Host      
      volumes:
        - name: web-storage
          persistentVolumeClaim:
            claimName: longhorn-rwx              
    ---
    apiVersion: v1
    kind: Service
    metadata:
      labels:
        app.kubernetes.io/name: web
      name: web
    spec:
      ports:
      - port: 80
        protocol: TCP
        targetPort: 80
      selector:
        app.kubernetes.io/name: web
      type: NodePort    
    EOF
    
### Funktionalität prüfen

Longhorn legt die Daten im Verzeichnis `/var/lib/longhorn/replicas/pvc-[ID]` als [ext4](https://de.wikipedia.org/wiki/Ext4) Image ab.

Mittels `debugfs` können die Verzeichnisse und Dateien angeschaut werden.

    sudo -i
    cd /var/lib/longhorn/replicas/pvc-[ID]
    debugfs volume-head-000.img
    ls -l 
    cat ....
    CTRL+d
    
### Longhorn UI

Das Longhorn-UI bietet eine benutzerfreundliche Oberfläche zur Verwaltung und Überwachung der Speichersysteme.

Port freischalten

    kubectl patch service longhorn-frontend --namespace longhorn-system -p '{"spec": {"type": "NodePort"}}'
    
Der URL ergibt sich dann aus dem Hostnamen und dem NodePort

    echo "http://"$(cat ~/work/server-ip)":$(kubectl get -n longhorn-system service longhorn-frontend -o=jsonpath='{ .spec.ports[0].nodePort }')/"  
    
**Alternative** `kubectl port-forward -n longhorn-system services/longhorn-frontend 80:80` 

### Aufträge

* Portiert Eure Microservices inkl. Dateiablage auf longhorn auf diese Umgebung
* Wie würdet Ihr ein Backup der Daten erstellen?

**Links**

- **Offizielle Dokumentation**: Für weitere Details und Best Practices konsultieren Sie die [offizielle Longhorn-Dokumentation](https://longhorn.io/docs/1.8.0/deploy/install/).

- **Fehlerbehebung**: Sollten während der Installation oder Nutzung Probleme auftreten, bietet die [Troubleshooting-Sektion der Longhorn-Dokumentation](https://longhorn.io/docs/1.8.0/troubleshooting/) hilfreiche Informationen.
