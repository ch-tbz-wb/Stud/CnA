###
#   Installation, Deinstallation microk8s mittels provisioner

resource "null_resource" "run_script_on_vms" {

  triggers = {
    fqdn = data.terraform_remote_state.infra.outputs.fqdn_control-plane-3
  }

  # SSH-Verbindung zur VM 
  connection {
    type        = "ssh"
    user        = "ubuntu"                 # Benutzername ist "ubuntu"
    private_key = file("~/.ssh/lerncloud") # Verwende den SSH-Schlüssel aus "~/.ssh/lerncloud"
    host        = self.triggers.fqdn       # VM-Name mit Suffix ".maas"
  }

  # Remote-Exec-Provisioner zum Ausführen des Shell-Skripts
  provisioner "remote-exec" {
    inline = [
      "curl -sfL https://get.k3s.io | INSTALL_K3S_EXEC=\"server --cluster-init\" sh -",
      "mkdir -p ~/.kube",
      "sudo cp /etc/rancher/k3s/k3s.yaml ~/.kube/config",
      "sudo chown $(id -u):$(id -g) ~/.kube/config",
      "sudo chmod 644 /etc/rancher/k3s/k3s.yaml",
      "sudo snap install helm --classic"
    ]
  }

  provisioner "remote-exec" {
    when = destroy
    inline = [
      "sudo /usr/local/bin/k3s-uninstall.sh"
    ]
    on_failure = continue
  }
}

