## k3s Kubernetes

K3s ist eine leichtgewichtige, produktionsbereite Kubernetes-Distribution, die speziell für Umgebungen mit eingeschränkten Ressourcen entwickelt wurde. Es wird von Rancher Labs (jetzt Teil von SUSE) bereitgestellt und ist ein beliebtes Tool für Edge-Computing, IoT-Geräte und lokale Entwicklungsumgebungen.

### k3s installieren

Nach dem Ausführen der `terraform` Befehle im Verzeichnis [01-infra](../01-infra)

    terraform workspace new [KVM-Host]
    terraform init
    terraform apply -auto-approve
    
### k3s deinstallieren

    terraform destroy -auto-approve

    