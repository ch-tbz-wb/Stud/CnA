02-k3s Kubernetes
==========

Umgebung zum Modul: [CNA - Cloud-native Advanced](https://gitlab.com/ch-tbz-wb/Stud/CnA).

SSH-Zugriff
-----------    
    
    ssh -i ~/.ssh/lerncloud ubuntu@${fqdn}  

Dashboard
---------

Das Kubernetes Dashboard ist wie folgt erreichbar.

    https://${fqdn}:8443 oder https://${ip}:8443 
    
Shell in a Box
--------------

Als Alternative zu ssh steht eine Shell, via Browser, zur Verfügung.

    https://${fqdn}:4200 oder https://${ip}:4200

Beispiele
---------

Die Umgebung beinhaltet eine Vielzahl von Beispielen als Juypter Notebooks. Die Jupyter Notebook Oberfläche ist wie folgt erreichbar:

    http://${fqdn}:32188/tree/CnA/2_Unterrichtsressourcen oder http://${ip}:32188/tree/CnA/2_Unterrichtsressourcen 