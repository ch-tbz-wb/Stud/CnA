### 01 Infrastruktur

Grundinfrastruktur, d.h. die VM mit minimaler Software
* NFS, /data Verzeichnis
* VPN
* docker und container Tools wie podman etc.
* Jupyter Notebook
* Shell in a Box
* Utilities wie: hey (lasttest), yq (wie jq aber auch für YAML), kind (K8s Cluster in Docker bauen), etc.

Erstellbar mittels `terraform`.
    
#### Terraform

Der Terraform Workspace wird verwendet um auf den richtigen KVM-Host zuzusteuern, deshalb ist zuerst der Terraform Workspace zu ändern:

    terraform workspace new aws
    terraform init
    terraform apply -auto-approve
 
**Hinweis**: zuerst ist in AWS Academy das Lab zu starten und die AWS `~/.aws/credentials` zu setzen.


