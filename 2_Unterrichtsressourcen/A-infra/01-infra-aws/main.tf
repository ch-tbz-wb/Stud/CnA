###
#   Ressourcen
#

# K3s Umgebung

module "control-plane-1" {

  source = "git::https://github.com/mc-b/terraform-lerncloud-aws"

  module  = "cna-${terraform.workspace}-microk8s"
  cores   = 4
  memory  = 8
  storage = 48

  userdata = "cloud-init-cna-microk8s.yaml"
  ports   = [22, 80, 443, 16443, 25000, 2049, 4200]
}

# K0s Umgebung
module "control-plane-2" {

  source  = "git::https://github.com/mc-b/terraform-lerncloud-aws"
  module  = "cna-${terraform.workspace}-k0s"
  cores   = 4
  memory  = 8
  storage = 32
  ports   = [22, 80, 443, 16443, 25000, 2049, 4200]

  userdata = "cloud-init-cna-k0s.yaml"
}

# frei Verfügbare Umgebung, z.B. für kind, talos etc.
module "control-plane-3" {

  source  = "git::https://github.com/mc-b/terraform-lerncloud-aws"
  module  = "cna-${terraform.workspace}-k3s"
  cores   = 4
  memory  = 8
  storage = 32
  ports   = [22, 80, 443, 16443, 25000, 2049, 4200]

  userdata = "cloud-init-cna-k3s.yaml"
}



