###
#   Outputs wie IP-Adresse und DNS Name

output "ip_control-plane-1" {
  value       = [module.control-plane-1.ip_vm]
  description = "The IP address of the server instance."
}

output "fqdn_control-plane-1" {
  value       = module.control-plane-1.fqdn_vm
  description = "The FQDN of the server instance."
}

output "fqdn_control-plane-2" {
  value       = module.control-plane-2.fqdn_vm
  description = "The FQDN of the server instance."
}

output "ip_control-plane-2" {
  value = [module.control-plane-2.ip_vm]
}


output "fqdn_control-plane-3" {
  value       = module.control-plane-3.fqdn_vm
  description = "The FQDN of the server instance."
}

output "ip_control-plane-3" {
  value       = [module.control-plane-3.ip_vm]
  description = "The IP address of the server instance."
}

output "intro" {
  value = templatefile("INTRO.md", {
    ip     = module.control-plane-1.ip_vm
    fqdn   = module.control-plane-1.fqdn_vm
    ip-2   = module.control-plane-2.ip_vm
    fqdn-2 = module.control-plane-2.fqdn_vm
    ip-3   = module.control-plane-3.ip_vm
    fqdn-3 = module.control-plane-3.fqdn_vm
    }
  )
}