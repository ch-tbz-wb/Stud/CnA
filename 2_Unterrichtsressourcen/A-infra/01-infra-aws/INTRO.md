01-Infra AWS
========
    
SSH-Zugriff
-----------    

microk8s Umgebung

    ssh -i ~/.ssh/lerncloud ubuntu@${fqdn}      
    
K0s Umgebung
    
    ssh -i ~/.ssh/lerncloud ubuntu@${fqdn-2}  
    
K3s Umgebung

    ssh -i ~/.ssh/lerncloud ubuntu@${fqdn-3}      
    
Dashboard
---------

Das Kubernetes Dashboard von microk8s ist wie folgt erreichbar.

    https://${fqdn}:8443 oder https://${ip}:8443    
    
Bei k0s und k3s muss es nachträglich, manuell installiert werden.     

Beispiele
---------

Die Umgebung beinhaltet eine Vielzahl von Beispielen als Juypter Notebooks. Die Jupyter Notebook Oberfläche ist wie folgt erreichbar:

    microk8s : http://${fqdn}:32188/tree/CnA/2_Unterrichtsressourcen/A-infra/README.ipynb 
    k0s      : http://${fqdn-2}:32188/tree/CnA/2_Unterrichtsressourcen/A-infra/README.ipynb 
    k3s      : http://${fqdn-3}:32188/tree/CnA/2_Unterrichtsressourcen/A-infra/README.ipynb          