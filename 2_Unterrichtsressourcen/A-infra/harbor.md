## Harbor
 
Wir Installieren bzw. Starten harbor mittels docker-compose. 
 
    sudo apt-get install docker-compose -y
 
    wget https://github.com/goharbor/harbor/releases/download/v2.12.2/harbor-offline-installer-v2.12.2.tgz
    tar xzf harbor-offline-installer-v2.12.2.tgz
    cd harbor
    
Dann brauchen wir die TLS Zertifikate vom Cert-Manager.
 
    kubectl get secret selfsigned-cert-secret -n m01-ch -o jsonpath="{.data.tls\.crt}" | base64 --decode > tls.crt
    kubectl get secret selfsigned-cert-secret -n m01-ch -o jsonpath="{.data.tls\.key}" | base64 --decode > tls.key
    
Zur Kontrolle schauen wir uns den Inhalt des Zertifikates an

    openssl x509 -in tls.crt -text -noout    
    
Damit die richtigen Zertifikate verwendet werden und es zu keinen Port Konflikten mit Kubernetes kommt müssen wir die Konfiguration von Harbor anpassen

    cp harbor.yml.tmpl harbor.yml
    
Und folgende Einträge ändern    

    hostname: reg.mydomain.com ===> ändern auf ch.cloud-hf-08-edutbz.com
    
    # http related config
    http:
      # port for http, default is 80. If https enabled, this port will redirect to https port
      port: 80 ==> ändern auf 9090
    
    # https related config
    https:
      # https port for harbor, default is 443
      port: 443 ==> ändern auf 9443
      # The path of cert and key files for nginx
      certificate: /your/certificate/path ===> ändern auf /home/ubuntu/harbor/tls.crt
      private_key: /your/private/key/path ===> ändern auf /home/ubuntu/harbor/tls.key
      
Und Harbor starten

    sudo ./install.sh --with-trivy
    
Harbor ist dann auf https://ch.cloud-hf-08-edutbz.com:9443 erreichbar. User: admin, Password: Harbor12345.

Wäre das Zertifikat von einer CA bestätigt, würde auch das Anmelden via `docker` funktionieren.

    docker login ch.cloud-hf-08-edutbz.com:9443
    
**Hinweis:** `cloud-hf-08` durch euren KVM-Host ersetzen.    
    
### Docker: Selbst signiertes Zertifikat vertrauenswürdig machen

* Docker erwartet standardmässig CA-Zertifikate (Certificate Authority Certificates) in einem bestimmten Verzeichnis, damit ein Registry-Zertifikat als sicher eingestuft wird.

CA Zertifikat extrahieren vom cert-manager

    kubectl get secret selfsigned-cert-secret -n m01-ch -o jsonpath="{.data.ca\.crt}" | base64 --decode > ca.crt
     
* Docker sucht in /etc/docker/certs.d/<REGISTRY_HOST>:<PORT> nach Zertifikaten, wenn du Images von <REGISTRY_HOST>:<PORT> beziehst.     
     
Passendes Verzeichnis anlegen

    sudo mkdir -p /etc/docker/certs.d/ch.cloud-hf-08-edutbz.com:9443
    sudo cp ca.crt /etc/docker/certs.d/ch.cloud-hf-08-edutbz.com:9443/ca.crt
    
Damit Docker das Zertifikat lädt, muss docker und Harbor neu gestartet werden:

    sudo docker-compose down -v
    sudo systemctl restart docker
    sudo ./install.sh --with-trivy    

Jetzt sollte `docker login` ohne Fehlermeldung funktionieren.

    docker login ch.cloud-hf-08-edutbz.com:9443
    
### microk8s: Selbst signiertes Zertifikat vertrauenswürdig machen (nicht getestet)    

    sudo mkdir -p /var/snap/microk8s/current/certs.d/ch.cloud-hf-08-edutbz.com:9443
    sudo cp ca.crt /var/snap/microk8s/current/certs.d/ch.cloud-hf-08-edutbz.com:9443/ca.crt

    microk8s stop
    microk8s start

### Aufträge

* Repliziert Eure Container Registries bzw. die Images nach Harbor
* Erstellt ein Container Image und legt es in die Harbor Registry ab

**Links**
* [Harbor Installer](https://goharbor.io/docs/2.12.0/install-config/run-installer-script/)           
 
 