###
#   Outputs wie IP-Adresse und DNS Name

output "ip_control-plane" {
  value = data.terraform_remote_state.infra.outputs.ip_control-plane-1
}

output "fqdn_control-plane" {
  value       = data.terraform_remote_state.infra.outputs.fqdn_control-plane-1
  description = "The FQDN of the server instance."
}

output "intro" {
  value = templatefile("INTRO.md", {
    ip    = element(tolist(data.terraform_remote_state.infra.outputs.ip_control-plane-1), 0)
    fqdn  = data.terraform_remote_state.infra.outputs.fqdn_control-plane-1
    ip2   = element(tolist(data.terraform_remote_state.infra.outputs.ip_control-plane-2), 0)
    fqdn2 = data.terraform_remote_state.infra.outputs.fqdn_control-plane-2
    ip3   = element(tolist(data.terraform_remote_state.infra.outputs.ip_control-plane-3), 0)
    fqdn3 = data.terraform_remote_state.infra.outputs.fqdn_control-plane-3
    }
  )
}