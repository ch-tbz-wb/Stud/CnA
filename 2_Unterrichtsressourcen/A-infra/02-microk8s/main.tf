###
#   Installation, Deinstallation microk8s mittels provisioner

resource "null_resource" "run_script_on_cp-1" {

  triggers = {
    fqdn = data.terraform_remote_state.infra.outputs.fqdn_control-plane-1
  }

  # SSH-Verbindung zur VM 
  connection {
    type        = "ssh"
    user        = "ubuntu"                 # Benutzername ist "ubuntu"
    private_key = file("~/.ssh/lerncloud") # Verwende den SSH-Schlüssel aus "~/.ssh/lerncloud"
    host        = self.triggers.fqdn       # VM-Name mit Suffix ".maas"
  }

  # Remote-Exec-Provisioner zum Ausführen des Shell-Skripts
  provisioner "remote-exec" {
    inline = [
      "curl -sfL https://raw.githubusercontent.com/mc-b/lerncloud/main/services/microk8s.sh | bash -",
      "curl -sfL https://raw.githubusercontent.com/mc-b/lerncloud/main/services/microk8saddons.sh | bash -"
    ]
  }

  provisioner "remote-exec" {
    when = destroy
    inline = [
      "sudo snap remove microk8s"
    ]
    on_failure = continue
  }
}

resource "null_resource" "run_script_on_cp-2" {

  triggers = {
    fqdn = data.terraform_remote_state.infra.outputs.fqdn_control-plane-2
  }

  # SSH-Verbindung zur VM 
  connection {
    type        = "ssh"
    user        = "ubuntu"                 # Benutzername ist "ubuntu"
    private_key = file("~/.ssh/lerncloud") # Verwende den SSH-Schlüssel aus "~/.ssh/lerncloud"
    host        = self.triggers.fqdn       # VM-Name mit Suffix ".maas"
  }

  # Remote-Exec-Provisioner zum Ausführen des Shell-Skripts
  provisioner "remote-exec" {
    inline = [
      "sudo snap install microk8s --classic",
      "sudo snap install kubectl --classic",
      "sudo usermod -a -G microk8s ubuntu",
      "sudo mkdir -p /home/ubuntu/.kube",
      "sudo microk8s config | sudo tee  /home/ubuntu/.kube/config",
      "sudo chown -f -R ubuntu:ubuntu /home/ubuntu/.kube",
      "sudo chmod 600 /home/ubuntu/.kube/config"
    ]
  }

  provisioner "remote-exec" {
    when = destroy
    inline = [
      "sudo snap remove microk8s"
    ]
    on_failure = continue
  }
}

resource "null_resource" "run_script_on_cp-3" {

  triggers = {
    fqdn = data.terraform_remote_state.infra.outputs.fqdn_control-plane-3
  }

  # SSH-Verbindung zur VM 
  connection {
    type        = "ssh"
    user        = "ubuntu"                 # Benutzername ist "ubuntu"
    private_key = file("~/.ssh/lerncloud") # Verwende den SSH-Schlüssel aus "~/.ssh/lerncloud"
    host        = self.triggers.fqdn       # VM-Name mit Suffix ".maas"
  }

  # Remote-Exec-Provisioner zum Ausführen des Shell-Skripts
  provisioner "remote-exec" {
    inline = [
      "sudo snap install microk8s --classic",
      "sudo snap install kubectl --classic",
      "sudo usermod -a -G microk8s ubuntu",
      "sudo mkdir -p /home/ubuntu/.kube",
      "sudo microk8s config | sudo tee  /home/ubuntu/.kube/config",
      "sudo chown -f -R ubuntu:ubuntu /home/ubuntu/.kube",
      "sudo chmod 600 /home/ubuntu/.kube/config"
    ]
  }

  provisioner "remote-exec" {
    when = destroy
    inline = [
      "sudo snap remove microk8s"
    ]
    on_failure = continue
  }
}

