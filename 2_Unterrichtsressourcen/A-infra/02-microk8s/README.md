## Microk8s Kubernetes

MicroK8s ist eine leichtgewichtige, produktionsbereite Kubernetes-Distribution, die von Canonical entwickelt wurde. Sie eignet sich ideal für Entwickler, die eine schnelle und einfache Möglichkeit suchen, Kubernetes in einer lokalen oder kleinen Umgebungen zu testen oder zu betreiben.

### Microk8s installieren

Nach dem Ausführen der `terraform` Befehle im Verzeichnis [01-infra](../01-infra)


    terraform workspace new [KVM-Host]
    terraform init
    terraform apply -auto-approve
    
### Microk8s deinstallieren

    terraform destroy -auto-approve

    