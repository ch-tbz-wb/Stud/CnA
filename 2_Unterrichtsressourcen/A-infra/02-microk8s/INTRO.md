02-microk8s Kubernetes
==========

Umgebung zum Modul: [CNA - Cloud-native Advanced](https://gitlab.com/ch-tbz-wb/Stud/CnA).

SSH-Zugriff
-----------    
    
    ssh -i ~/.ssh/lerncloud ubuntu@${fqdn}  

Kubernetes Cluster 
------------------

Es werden einzelne VMs erstellt. Diese können wie folgt zu einen Kubernetes Cluster zusammengefügt werden.
    
    JOIN=$(ssh -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -i ~/.ssh/lerncloud ubuntu@${fqdn} -- microk8s add-node --token-ttl 3600 | head -2 | tail -1)
    
    # Join Worker(s)
    ssh -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -i ~/.ssh/lerncloud ubuntu@${fqdn2} -- sudo $JOIN
    ssh -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -i ~/.ssh/lerncloud ubuntu@${fqdn3} -- sudo $JOIN
    
Zusätzlich müssen sich die Worker, via NFS, mit dem Master verbinden. Ansonsten funktionieren die Beispiele, die Persistenz verwenden, nicht.

    ssh -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -i ~/.ssh/lerncloud ubuntu@${fqdn2} "sudo mkdir -p /data; sudo mount -t nfs ${fqdn}:/data /data"
    ssh -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -i ~/.ssh/lerncloud ubuntu@${fqdn3} "sudo mkdir -p /data; sudo mount -t nfs ${fqdn}:/data /data" 
    
Falls die DNS Auflösung nicht funktoniert, können alternativ die IP-Adressen verwendet werden:

    JOIN=$(ssh -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -i ~/.ssh/lerncloud ubuntu@${ip} -- microk8s add-node --token-ttl 3600 | head -2 | tail -1)
    
    # Join Worker(s)
    ssh -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -i ~/.ssh/lerncloud ubuntu@${ip2} -- sudo $JOIN
    ssh -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -i ~/.ssh/lerncloud ubuntu@${ip3} -- sudo $JOIN
    
    ssh -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -i ~/.ssh/lerncloud ubuntu@${ip2} "sudo mkdir -p /data; sudo mount -t nfs ${ip}:/data /data"
    ssh -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -i ~/.ssh/lerncloud ubuntu@${ip3} "sudo mkdir -p /data; sudo mount -t nfs ${ip}/data /data"     

**Tipp**: DNS und IP-Adresse in /etc/hosts, bzw. C:\Windows\system32\drivers\etc\hosts eintragen.
    
Dashboard
---------

Das Kubernetes Dashboard ist wie folgt erreichbar.

    https://${fqdn}:8443 oder https://${ip}:8443 
    
Shell in a Box
--------------

Als Alternative zu ssh steht eine Shell, via Browser, zur Verfügung.

    https://${fqdn}:4200 oder https://${ip}:4200

Beispiele
---------

Die Umgebung beinhaltet eine Vielzahl von Beispielen als Juypter Notebooks. Die Jupyter Notebook Oberfläche ist wie folgt erreichbar:

    http://${fqdn}:32188/tree/CnA/2_Unterrichtsressourcen oder http://${ip}:32188/tree/CnA/2_Unterrichtsressourcen 