# Service Mesh

Der Begriff Service-Mesh beschreibt das Netzwerk von Microservices und die Wechselwirkungen zwischen ihnen.

Je grösser und komplexer ein Service-Mesh wird, desto schwieriger ist es, es zu verstehen und zu verwalten.

Zu den Anforderungen zählen Erkennung, Lastenausgleich, Fehlerbehebung, Metriken und Überwachung.

Ein Service-Mesh hat häufig komplexere betriebliche Anforderungen, wie A/B-Tests (Bewertung zweier Varianten), Canary-Rollouts, Beschränkungen, Zugangskontrolle und End-to-End-Authentifizierung.

### Links

* [Ïstio](https://istio.io/latest/about/service-mesh/)
* [Bookinfo aus Docker und Kubernetes Kurs](https://github.com/mc-b/duk/blob/master/data/jupyter/demo/ServiceMesh-Bookinfo.ipynb)
* [Travel Demo aus Docker und Kubernetes Kurs](https://github.com/mc-b/duk-demo/blob/master/data/jupyter/demo/ServiceMesh-TravelDemo.ipynb) braucht externen DNS Server und öffentliche IP für Kubernetes Cluster.
