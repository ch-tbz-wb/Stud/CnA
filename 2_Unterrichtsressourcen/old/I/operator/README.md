# Operator

Zuerst ist das Image wie [hier](https://github.com/flant/shell-operator#quickstart) beschrieben erstellt werden.

Dannach folgt die `Clusterrole`, wo festlegt, was `--verb=get,watch,list --resource=pods` überwacht werden soll. Alle Befehle zusammen:

    kubectl create namespace example-monitor-pods
    kubectl create serviceaccount monitor-pods-acc --namespace example-monitor-pods
    kubectl create clusterrole monitor-pods --verb=get,watch,list --resource=pods
    kubectl create clusterrolebinding monitor-pods --clusterrole=monitor-pods --serviceaccount=example-monitor-pods:monitor-pods-acc
    kubectl -n example-monitor-pods apply -f shell-operator-pod.yaml

Nach dem Starten eines Pods, sollte das Log um ein paar Einträge reicher sein

    kubectl run hello-world --image registry.gitlab.com/mc-b/misegr/hello-world

    kubectl -n example-monitor-pods logs po/shell-operator

Ausgabe

    {"binding":"kubernetes","event.id":"a9db07e4-a226-4dfb-beee-de2a03813e55","level":"info","msg":"queue task HookRun:main:kubernetes:pods-hook.sh:kubernetes"}
    {"binding":"kubernetes","event":"kubernetes","hook":"pods-hook.sh","level":"info","msg":"Execute hook","queue":"main","task":"HookRun"}
    {"binding":"kubernetes","event":"kubernetes","hook":"pods-hook.sh","level":"info","msg":"Pod 'hello-world' added","output":"stdout","queue":"main","task":"HookRun"}
    {"binding":"kubernetes","event":"kubernetes","hook":"pods-hook.sh","level":"info","msg":"Hook executed successfully","queue":"main","task":"HookRun"}

Aufräumen

    kubectl delete ns example-monitor-pods
    kubectl delete clusterrole monitor-pods
    kubectl delete clusterrolebinding monitor-pods
    kubectl delete pods/hello-world

### Links

* [GitHub](https://github.com/flant/shell-operator)
* [Articles & talks](https://github.com/flant/shell-operator#articles--talks)

### Operator erstellen

Der Operator wurde mittels des [Shell Operators](https://github.com/flant/shell-operator) erstellt.

Dazu wird ein [Dockerfile](Dockerfile) und eine [Shell Script](pods-hook.sh) benötigt.

Nach dem Erstellen des Container Images (`docker build`) und Abstellen in einer Registry (`docker push`) wird der Operator mittels dieser [YAML Datei](shell-operator-pod.yaml) gestartet.