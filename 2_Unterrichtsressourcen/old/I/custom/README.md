Custom Resources inkl. Operator
-------------------------------

Erweitert Kubernetes um eine Custom Resource [MyService/education.tbz.ch/v1](resourcedefinition.yaml) inkl. eines Operators welche die Abhandlung von "Add" und "Delete" übernimmt.

    apiVersion: "education.tbz.ch/v1"
    kind: MyService
    metadata:
        name: hello-world
    spec:
        image: registry.gitlab.com/mc-b/misegr/hello-world:V1.0
        replicas: 3
        port: 80

Bei jedem Erstellen von `MyService/education.tbz.ch/v1` wird ein `deployment` mit einer Anzahl `peplicas` und ein `service` erzeugt.

Zuerst muss die Resource, dann die Service Accounts und Anschliessend der Operator erstellt werden:

    kubectl apply -f resourcedefinition.yaml
    kubectl apply -f myservice-operator-rbac.yaml
    kubectl apply -f myservice-operator.yaml

Anschliessend eine YAML Datei für die Resource `MyService/education.tbz.ch/v1` erstellen (siehe oben) und Resource erstellen

    kubectl apply -f myservice.yaml

Mittels `kubectl get all` kann das Ergebnis überprüft werden 

    NAME                               READY   STATUS    RESTARTS   AGE
    pod/myservice-operator             1/1     Running   0          12m
    pod/hello-world-7c6cf57c5b-7nx7b   1/1     Running   0          7s
    pod/hello-world-7c6cf57c5b-4dsmp   1/1     Running   0          7s
    pod/hello-world-7c6cf57c5b-rsqhh   1/1     Running   0          7s

    NAME                   TYPE           CLUSTER-IP       EXTERNAL-IP   PORT(S)          AGE
    service/hello-world    LoadBalancer   10.152.183.213   <pending>     80:32247/TCP     7s

    NAME                           READY   UP-TO-DATE   AVAILABLE   AGE
    deployment.apps/hello-world    3/3     3            3           8s

    NAME                                     DESIRED   CURRENT   READY   AGE
    replicaset.apps/hello-world-7c6cf57c5b   3         3         3       8s

Neben dem Operator laufen drei Instanzen unseres Services, zusätzlich wurde ein `service` erstellt.

### Known issues

Namespaces werden nicht berücksichtigt.

### Operator erstellen

Der Operator wurde mittels des [Shell Operators](https://github.com/flant/shell-operator) erstellt.

Dazu wird ein [Dockerfile](Dockerfile) und eine [Shell Script](hooks/myservice-hook.sh) benötigt.

Nach dem Erstellen des Container Images (`docker build`) und Abstellen in einer Registry (`docker push`) wird der Operator mittels dieser [YAML Datei](myservice-operator.yaml) gestartet.

