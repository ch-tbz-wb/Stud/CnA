# Init Container

Init Container sind Spezialcontainer, die vor App-Containern in einem Pod ausgeführt werden.

### Links

* [Kubernetes Doku](https://kubernetes.io/docs/concepts/workloads/pods/init-containers/)
* [Hands-on aus Docker und Kubernetes Kurs](https://github.com/mc-b/duk/blob/master/data/jupyter/09-10-Init.ipynb)