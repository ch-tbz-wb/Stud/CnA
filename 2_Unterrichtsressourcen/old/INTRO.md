Kubernetes
==========

Umgebung zum Modul: [CNA - Cloud-native Advanced](https://gitlab.com/ch-tbz-wb/Stud/CnA).

Es werden einzelne VMs erstellt. Diese können wie folgt zu einen Kubernetes Cluster zusammengefügt werden.

    JOIN=$(ssh -i ~/.ssh/lerncloud ubuntu@${fqdn} -- microk8s add-node --token-ttl 3600 | grep worker | tail -1)
    
    # Join Worker(s)
    ssh -i ~/.ssh/lerncloud ubuntu@${worker_01_fqdn} -- $JOIN
    ssh -i ~/.ssh/lerncloud ubuntu@${worker_02_fqdn} -- $JOIN
    ssh -i ~/.ssh/lerncloud ubuntu@${worker_03_fqdn} -- $JOIN
    
**Hinweis**: Funktioniert nur bei einer OpenVPN Verbindung. Bei WireGuard sind die IP-Adressen der VMs zu verwenden.   

    JOIN=$(ssh -i ~/.ssh/lerncloud ubuntu@${ip} -- microk8s add-node --token-ttl 3600 | grep worker | tail -1)
    
    # Join Worker(s)
    ssh -i ~/.ssh/lerncloud ubuntu@${worker_01_ip} -- $JOIN
    ssh -i ~/.ssh/lerncloud ubuntu@${worker_02_ip} -- $JOIN
    ssh -i ~/.ssh/lerncloud ubuntu@${worker_03_ip} -- $JOIN 
    
Dashboard
---------

Das Kubernetes Dashboard ist wie folgt erreichbar.

    https://${fqdn}:8443 oder https://${ip}:8443 
    
Shell in a Box
--------------

Als Alternative zu ssh steht eine Shell, via Browser, zur Verfügung.

    https://${fqdn}:4200 oder https://${ip}:4200

Beispiele
---------

Die Umgebung beinhaltet eine Vielzahl von Beispielen als Juypter Notebooks. Die Jupyter Notebook Oberfläche ist wie folgt erreichbar:

    http://${fqdn}:32188/tree/CnA/2_Unterrichtsressourcen oder http://${ip}:32188/tree/CnA/2_Unterrichtsressourcen 