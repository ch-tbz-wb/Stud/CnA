# DaemonSet

Ein DaemonSet stellt sicher, dass alle (oder einige) Nodes eine Kopie eines Pods ausführen. Wenn dem Cluster Knoten hinzugefügt werden, werden ihnen Pods hinzugefügt.

Wenn Knoten aus dem Cluster entfernt werden, werden diese Pods von der Garbage Collection erfasst. Durch das Löschen eines DaemonSets werden die von ihm erstellten Pods bereinigt.

Einige typische Anwendungen eines DaemonSets sind:
* Ausführen eines Cluster-Storage-Daemons auf jedem Knoten
* Ausführen eines Daemons zum Sammeln von Protokollen auf jedem Knoten
* Ausführen eines Knotenüberwachungs-Daemons auf jedem Knoten

### Vorarbeiten

Docker speichert seine Logfile in `/var/lib/docker/containers`. Microk8s aber in `/var/log/pods`.

Deshalb ist, auf jeder Node, zuerst das Verzeichnis bzw. der Link `/var/lib/docker/containers` zu erstellen.

    ssh ubuntu@<node>
    sudo -i
    mkdir /var/lib/docker
    ln -s /var/log/pods/ /var/lib/docker/containers

### Beispiel

Beispiel basierend auf [DaemonSet](https://kubernetes.io/docs/concepts/workloads/controllers/daemonset/).

    kubectl apply -f https://k8s.io/examples/controllers/daemonset.yaml

Es müssen, gleiche viele `fluent` Pods, wie Nodes aktiv sein. Auf jeder Node muss ein Pods laufen.    

    kubectl get pods -n kube-system -o wide 

### Links

* [Kubernetes Doku](https://kubernetes.io/docs/concepts/workloads/controllers/daemonset/)
* [Microk8s Troubleshooting](https://microk8s.io/docs/troubleshooting)
* [Manage Cluster Daemons](https://kubernetes.io/docs/tasks/manage-daemon/)
