# Jobs

Jobs sind nützlich für grosse Berechnungen und batchorientierte Aufgaben. 

Mit einem Job können Sie unabhängige, aber verwandte Arbeitsaufgaben parallel ausführen: E-Mails senden, Frames rendern, Dateien transcodieren, Datenbankschlüssel scannen usw. Jobs sind jedoch nicht für intensiv kommunizierende parallele Prozesse wie kontinuierliche Hintergrundprozesse ausgelegt.

## CronJobs

Beispiel für einen CronJobs aus [Run Jobs](https://kubernetes.io/docs/tasks/job/):

    apiVersion: batch/v1
    kind: CronJob
    metadata:
    name: hello
    spec:
    schedule: "* * * * *"
    jobTemplate:
        spec:
        template:
            spec:
            containers:
            - name: hello
                image: busybox:1.28
                imagePullPolicy: IfNotPresent
                command:
                - /bin/sh
                - -c
                - date; echo Hello from the Kubernetes cluster
            restartPolicy: OnFailure

Starten des Jobs

    kubectl apply -f https://k8s.io/examples/application/job/cronjob.yaml

Anzeigen der Starts des Jobs. Beenden mit Ctrl+c

    kubectl get jobs --watch

Aufräumen

    kubectl delete -f https://k8s.io/examples/application/job/cronjob.yaml

## Jobs

Kubernetes verwaltet Pods, die als Jobs ausgeführt werden.

Durch die Angabe von `parallelism` (Gleichzeitigkeit), und `completions` (Anzahl Jobs) können Jobs aufgeteilt und parallel gestartet werden. Ohne dabei den Cluster unnötig zu belasten.

Das nachfolgende Beispiel aus [Jobs Konzept](https://kubernetes.io/docs/concepts/workloads/controllers/job/), startet jeweils 2 parallele Pods und stellt sicher, dass 6 Pods durchlaufen.

    apiVersion: batch/v1
    kind: Job
    metadata:
      name: pi
    spec:
      parallelism: 2
      completions: 6
      template:
        spec:
          containers:
          - name: pi
            image: perl:5.34.0
            command: ["perl",  "-Mbignum=bpi", "-wle", "print bpi(2000)"]
          restartPolicy: Never
      backoffLimit: 4

Starten mittels

    kubectl apply -f 2_Unterrichtsressourcen/A/job.yaml

Anzeigen der Jobs/Pods und deren Ausgabe

    kubectl get pods,jobs
    kubectl logs jobs/pi

Aufräumen

    kubectl delete -f 2_Unterrichtsressourcen/A/job.yaml

### Links

* [Jobs Konzept](https://kubernetes.io/docs/concepts/workloads/controllers/job/)
* [Run Jobs](https://kubernetes.io/docs/tasks/job/)
* [CronJobs](https://kubernetes.io/docs/concepts/workloads/controllers/cron-jobs)
