# Monitoring

Manuelles Überwachen ständig sterbender, neu gestarteter sowie veränderter Services, Container und sonstiger Ressourcen im Container-Cluster stellt komplett neue Anforderungen an das Monitoring. 

Setups wie in konventionellen Sysadmin-Umgebungen auf Blech oder VMs sind hier weder praktikabel oder sinnvoll noch in irgendeiner Form realisierbar.

### Installation

Die Installation des Open Source Monitoring Stack erfolgt am einfachsten mit `helm`

Dazu erstellen wir zuerst einen eigenen Namespace und verwenden dann das [Helm Chart](https://artifacthub.io/packages/helm/prometheus-community/kube-prometheus-stack) von [Open Source Monitoring Stack](https://github.com/prometheus-operator/kube-prometheus)

    kubectl create namespace monitoring
    helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
    helm repo update
    helm install monitoring prometheus-community/kube-prometheus-stack -n monitoring

Der [Open Source Monitoring Stack](https://github.com/prometheus-operator/kube-prometheus) beinhaltet:

* Grafana
* Prometheus 
* Alertmanager 

Um auf die UI zugreifen zu können müssen wir die Ports der User Interfaces, von auf ClusterIP auf LoadBalancer ändern:

    kubectl -n monitoring get service -l app.kubernetes.io/name=grafana -o yaml | sed 's/ClusterIP/LoadBalancer/g' | kubectl apply -f -
    kubectl -n monitoring get service -l app=kube-prometheus-stack-prometheus -o yaml | sed 's/ClusterIP/LoadBalancer/g' | kubectl apply -f -
    kubectl -n monitoring get service -l app=kube-prometheus-stack-alertmanager -o yaml | sed 's/ClusterIP/LoadBalancer/g' | kubectl apply -f -

Die weiter geleiteten Ports finden lassen sich durch die Ausgabe der Service herausfinden:

    kubectl -n montoring get services

Grafana UI Einloggen mittels **User/Password admin/prom-operator**. Auf der linken Seite kann zwischen einer Reihe von vorbereitenden Dashboards ausgewählt werden, z.B. ../Cluster.

In der Prometheus Oberfläche kann mittels der Abfragesprache PromQL gezielt Ressourcen ausgewählt werden, z.B. durch Query von `apiserver_storage_objects`.

Der Alertmanager dient zum Verarbeiten von Warnungen. Für ein Beispiel siehe [Notifikation Beispiel](https://prometheus.io/docs/alerting/notification_examples/).

### Links

* [Prometheus](https://prometheus.io/)
* [Open Source Monitoring Stack](https://github.com/prometheus-operator/kube-prometheus)
* [Helm Chart](https://artifacthub.io/packages/helm/prometheus-community/kube-prometheus-stack)
* [Hands-on aus Docker und Kubernetes Kurs](https://github.com/mc-b/duk-demo/blob/master/data/jupyter/demo/Prometheus.ipynb) - ohne Installation Prometheus
* [Vorbereitete Grafana Dashboards](https://github.com/kubernetes-monitoring/kubernetes-mixin)
* [Notification Beispiel](https://prometheus.io/docs/alerting/latest/notification_examples/)
