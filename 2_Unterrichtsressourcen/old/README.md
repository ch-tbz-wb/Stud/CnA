# Unterrichtsressourcen 

In einem ersten Schritt werden, bis jetzt nur am Rande behandelte, Kubernetes Ressourcen vertieft.

* [A - Jobs](A/)
* [B - Health Probe Pattern](B/)
* [C - Init Container](C/)
* [D - Horizontal Pod Autoscaler](D/)
* [E - Zugriffssteuerung](E/) (Role Based Access, RBAC)
* [F - DaemonSet](F/)
* [G - StatefulSet](G/)

**Weitergehende Konzepte (Add-ons)**

Anschliessend werden weitergehende Konzepte behandelt.

* [H - Helm - der Paketmanager für Kubernetes](H/)
* [I - Operator Pattern](I/)
* [J - Monitoring](J/)
* [K - Logging](K/)
* [L - Service Mesh](L/)
* [M - Serverless (Function as a Service, FaaS)](M/)
* [N - Network Policies](https://kubernetes.io/docs/concepts/services-networking/network-policies/)

## Infrastruktur erstellen
    
    git clone https://gitlab.com/ch-tbz-wb/Stud/cna
    cd cna/2_Unterrichtsressourcen
    
    terraform init
    
    terraform plan
    
    terraform apply -auto-approve 
    
Für mehrere Studenten

    export TF_VAR_host_no=10
    for stud in 01 02 03 04 05 06
    do
        terraform workspace new ws${stud}
        terraform workspace select ws${stud}
        terraform init
        terraform apply -auto-approve
        TF_VAR_host_no=$((TF_VAR_host_no + 6))
    done
        
**Hinweis**: falls MAAS sich mit Fehler meldet, einloggen in den entsprechende KVM-Host und VMs mittels `virsh list --all` und `virsh undefine xxx` manuell weglöschen.

Der Zugriff, ohne Password, kann mittels [lerncloud](https://github.com/mc-b/lerncloud/raw/main/ssh/lerncloud) SSH-Key erfolgen.

    ssh -i ~/.ssh/lerncloud ubuntu@ws01-10-cna-controlplane-01.maas
    
### Container Image erstellen (nicht für Produktive Umgebungen)

Auf der `controlplan-01` ist neben `docker` und `podman` auch `podman-compose` installiert.

Mit allen drei können Container Images erstellt werden. Diese sind dann im jeweiligen Image Verzeichnis des Tools abgelegt und für `microk8s` nicht direkt erreichbar.

**Lösung**: Container Images ins `tar` Format exportieren und in `microk8s` importieren.

    docker save catalog -o catalog.tar
    microk8s ctr catalog import catalog.tar
    
Beim Starten mit Kubernetes die `IfNotPresent` Policy verwenden.

    kubectl run catalog --image catalog --image-pull-policy="IfNotPresent" --restart=Never
    
Wurde schon ein Cluster erstellt, d.h. es sind mehrere Kubernetes Nodes vorhanden, ist der Container explizit auf der ersten Node zu starten:

    kubectl run catalog --image catalog --image-pull-policy="IfNotPresent" --restart=Never \
    --overrides='{ "apiVersion": "v1", "spec": { "nodeName": "ws01-10-cna-controlplane-01" } }'     
               
      
### Aufräumen
       
    export TF_VAR_host_no=10
    for stud in 01 02 03 04 05 06
    do
        terraform workspace select ws${stud}    
        terraform destroy -auto-approve
        TF_VAR_host_no=$((TF_VAR_host_no + 6))
    done 
    
### Fragen und Antworten

**Ich habe mein State Datei von Terraform gelöscht, wie kann ich diese wieder herstellen**

Die State Datei bzw. dessen Einträge können mittels `terraform import` wiederhergestellt werden. 

Beispiel

    terraform workspace new ws01
    terraform workspace select ws01
    terraform init

    export TF_VAR_host_no=10
    terraform import module.controlplane-01.maas_vm_instance.vm ws01-$((TF_VAR_host_no))-cna-controlplane-01.maas
    terraform import module.worker-01.maas_vm_instance.vm ws01-$((TF_VAR_host_no + 3))-cna-worker-01.maas
    terraform import module.worker-02.maas_vm_instance.vm ws01-$((TF_VAR_host_no + 4))-cna-worker-02.maas
    terraform import module.worker-03.maas_vm_instance.vm ws01-$((TF_VAR_host_no + 5))-cna-worker-03.maas   
    
Kontrollieren mittels

    terrform show     
    
## Weitere Microservices

Wo finde ich weitere Applikationen und Microservices zum Üben

* [Monolithische Applikation](https://github.com/mc-b/duk/blob/master/data/jupyter/autoshop/IntroAutoShop.ipynb)
* [Einfacher WebShop implementiert als Microservice-Applikation](https://github.com/mc-b/duk/blob/master/data/jupyter/autoshop-ms/IntroAutoShop-ms.ipynb)              

## AWS

MAAS Modul deaktivieren und AWS aktivieren.

    sed -i -e 's|  source *=* "git::https://github.com/mc-b/terraform-lerncloud-maas"|  #source = "git::https://github.com/mc-b/terraform-lerncloud-maas"|' \
           -e 's|  #source *=* "git::https://github.com/mc-b/terraform-lerncloud-aws"|  source = "git::https://github.com/mc-b/terraform-lerncloud-aws"|' main.tf
           
Nach der Installation sind die Ports zwischen den AWS Netzwerken zu öffnen, dass das Overlay Netzwerk kommunizieren kann.  

    sed -i -e 's|  #source *=* "git::https://github.com/mc-b/terraform-lerncloud-maas"|  source = "git::https://github.com/mc-b/terraform-lerncloud-maas"|' \
           -e 's|  source *=* "git::https://github.com/mc-b/terraform-lerncloud-aws"|  #source = "git::https://github.com/mc-b/terraform-lerncloud-aws"|' main.tf         
