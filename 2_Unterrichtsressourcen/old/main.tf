###
#   Kubernetes Umgebung 1 x ControlPlan, 3 x Worker
#

module "cna" {
  # 1 Umgebung = 4 VMs
  # source = "git::https://github.com/mc-b/terraform-lerncloud-maas.git?ref=v2.0.0"
  # 1 Umgebung pro KVM-Host = KVM Host * 4
  source = "git::https://github.com/mc-b/terraform-lerncloud-lernmaas.git?ref=v2.0.0"

  machines = {
    controlplane-01 = {
      #hostname      = "${terraform.workspace}-${var.host_no}-cna-controlplane-01"
      hostname    = "cna-controlplane-01"
      description = "Kubernetes Master"
      userdata    = "cloud-init-cna-controlplane.yaml"
      cores       = 4
      storage     = 32
    },
    "worker-01" = {
      #hostname      = "${terraform.workspace}-${var.host_no + 3}-cna-worker-01"
      hostname = "cna-worker-01"
      userdata = "cloud-init-cna-worker.yaml"
    },
    "worker-02" = {
      # hostname      = "${terraform.workspace}-${var.host_no + 4}-cna-worker-02"
      hostname = "cna-worker-02"
      userdata = "cloud-init-cna-worker.yaml"
    },
    "worker-03" = {
      # hostname      = "${terraform.workspace}-${var.host_no + 5}-cna-worker-03"
      hostname = "cna-worker-03"
      userdata = "cloud-init-cna-worker.yaml"
    }
  }

  description = "Kubernetes Worker"
  cores       = 2
  memory      = 4
  storage     = 24

  # ssh, http, microk8s, shellinabox
  ports = [22, 80, 443, 16443, 25000, 4200]

  url = var.url
  key = var.key
  vpn = var.vpn
  
  # nur Modul lernmaas = Anzahl VMs per KVM-Host (Default = 1)
  # vm_per_host = 2

}