# Horizontale Pod-Autoscaler

Der horizontale Pod-Autoscaler skaliert automatisch die Anzahl der Pods in einem ReplicaSet, einem Deployment oder einem StatefulSet.

Die Skalierung kann anhand der beobachteten CPU-Auslastung oder mittels benutzerdefinierten Metriken erfolgen. Für Details siehe hier.

Die Metriken kommen von einem separat gestarteten Metric Server.

### Links

* [Kubernetes Doku](https://kubernetes.io/docs/tasks/run-application/horizontal-pod-autoscale/)
* [Hands-on aus Docker und Kubernetes Kurs](https://github.com/mc-b/duk/blob/master/data/jupyter/09-8-HorizontalPodAutoscaler.ipynb)