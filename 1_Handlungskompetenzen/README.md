# Handlungskompetenzen

## Kompetenzmatrix

| **Kompetenzenband**                  | **RP**                                                                                | **HZ**  | **Novizenkompetenz**                                                                 | **Fortgeschrittene Kompetenz**                                                        | **Kompetenz professionellen Handelns**                                                  | **Kompetenzexpertise**                                                                       |
|----------------------------------------|--------------------------------------------------------------------------------------|---------|-------------------------------------------------------------------------------------|---------------------------------------------------------------------------------------|----------------------------------------------------------------------------------------|------------------------------------------------------------------------------------------------|
| **CNA1**                               |                                                                                      |          |                                                                                      |                                                                                        |                                                                                          |                                                                                                                                     |
| **a) Health Probe Pattern**               | B12.3, B14.2                                                                         |     1    | Verstehen der grundlegenden Konzepte von Liveness und Readiness Probes.              | Konfiguration von einfachen Health Probes zur Überwachung des Zustands von Pods.       | Analyse und Optimierung von Health Probes zur Maximierung der Anwendungssicherheit.      | Entwicklung und Implementierung komplexer Health Probes für skalierbare und fehlerresistente Anwendungen.                           |
| **b) Init Container**                     | B14.1, B4.6                                                                          |    2     | Grundlegendes Verständnis von Init Containers und ihrer Funktion in Kubernetes Pods. | Verwendung von Init Containers, um einfache Vorbereitungsaufgaben zu erfüllen.         | Integration von Init Containers in komplexen Pod-Setups zur Automatisierung von Prozessen vor dem Start der Hauptanwendung.    | Entwicklung und Optimierung von Init-Container-Strategien für komplexe Workflows und hochspezifische Anwendungsanforderungen.       |
| **c) Zugriffssteuerung (RBAC)**           | B12.1, A2.1, A2.3                                                                    |   3      | Verstehen der Grundlagen von Role-Based Access Control in Kubernetes.                | Implementierung von RBAC für kleine Teams oder Projekte.                              | Verwaltung und Skalierung von RBAC für grössere Teams mit verschiedenen Zugriffsanforderungen.                                      | Entwurf und Implementierung von unternehmensweiten RBAC-Strategien für Sicherheit und Effizienz im Zugriff auf Kubernetes-Ressourcen. |
| **d) Helm - Paketmanager für Kubernetes** | B4.6, A2.5, C1.5                                                                           |     4    | Grundlegendes Verständnis von Helm und dessen Funktionsweise zur Paketverwaltung.    | Verwendung von Helm, um einfache Kubernetes-Anwendungen bereitzustellen und zu verwalten. | Erstellung und Verwaltung von Helm-Charts für komplexe Anwendungen mit vielen Konfigurationsmöglichkeiten.                     | Entwicklung, Versionierung und Verwaltung von benutzerdefinierten Helm-Charts für eine unternehmensweite Kubernetes-Strategie.       |
| **e) Operator Pattern**                   | B12.4, A3.3, A3.4, C1.5                                                                    |    5     | Verstehen der grundlegenden Funktionsweise von Kubernetes-Operatoren.               | Nutzung bestehender Operatoren zur Verwaltung von einfachen Aufgaben in Kubernetes.    | Entwicklung und Implementierung von Operatoren zur Automatisierung von komplexen Aufgaben und dem Lifecycle-Management von Anwendungen. | Entwurf und Entwicklung unternehmensspezifischer Operatoren zur vollständigen Automatisierung des Anwendungs-Lifecycle-Managements und fortlaufender Optimierung. |
| **CNA2**                               |                                                                                      |          |                                                                                      |                                                                                        |                                                                                          |                                                                                                                                     |
| **f) Monitoring**                        | B14.2, B4.6, C2.4                                                                          |     5    | Grundlegendes Verständnis von Monitoring-Tools wie Prometheus und Grafana.           | Konfiguration von Monitoring-Tools zur Überwachung von Ressourcen und Anwendungen.     | Einrichtung eines umfassenden Monitoring-Systems mit Alarmierung und Performance-Optimierung. | Entwicklung und Implementierung massgeschneiderter Monitoring-Lösungen für komplexe Anwendungen und Cluster-Architekturen.             |
| **g) Logging (Logfiles sammeln, filtern und auswerten)** | B12.3, B14.2, C2.4                                                                         |   6      | Verstehen der Grundlagen von Logging in Kubernetes-Umgebungen.                       | Implementierung von zentralisierten Logging-Systemen wie Fluentd oder ELK Stack.      | Erstellung von Filterregeln und Analyse-Tools, um Logs effizient zu sammeln und auszuwerten.  | Entwicklung von massgeschneiderten Logging-Strategien und Systemen zur kontinuierlichen Überwachung und Fehlersuche.                   |
| **h) Service Mesh**                      | B12.1, A2.5, A3.3                                                                    |   7      | Grundlegendes Verständnis von Service Meshes wie Istio oder Linkerd.                 | Implementierung eines Service Meshes zur Steuerung von Netzwerk-Traffic und Sicherheit. | Konfiguration und Management eines Service Meshes für zuverlässige Kommunikation und sichere Service-Verbindungen.                   | Entwicklung und Optimierung eines massgeschneiderten Service Meshes für hochverfügbare und sichere Microservice-Architekturen.         |
| **i) Serverless (FaaS)**                 | A3.1, A3.2, B4.6, C1.5                                                                     |    8     | Verstehen der Grundlagen von Function-as-a-Service (FaaS) und Serverless-Architekturen. | Implementierung einer Serverless-Plattform auf Kubernetes, z.B. mit OpenFaaS oder Knative. | Verwaltung und Skalierung von Serverless-Funktionen zur effizienten Ressourcennutzung und Performancesteigerung.                       | Design und Optimierung einer unternehmensweiten Serverless-Strategie zur Maximierung der Flexibilität und Kosteneffizienz.            |
| **j) Network Policies**                  | B12.4, B14.1, A2.1, A2.3                                                             |   9      | Grundlegendes Verständnis von Kubernetes Network Policies und deren Einsatzmöglichkeiten. | Implementierung von einfachen Network Policies zur Steuerung des Netzwerkverkehrs zwischen Pods. | Konfiguration komplexer Network Policies für mehr Sicherheit und Zugriffskontrolle innerhalb des Clusters.                          | Entwicklung einer umfassenden Sicherheitsstrategie durch massgeschneiderte Network Policies zur Steuerung des Datenverkehrs und Zugriffsschutz. |

* RP = Rahmenlehrplan
* HZ = Handlungsziele

## Modulspezifische Handlungskompetenzen

 * B4 Entwicklungsmethoden zur Lösung von ICT-Problemen und Entwicklung von ICT-Innovationen zielführend einsetzen
   * B4.6 Aktuelle technologiebasierte Entwicklungswerkzeuge einsetzen (Niveau: 3)

 * B12 System- und Netzwerkarchitektur bestimmen
   * B12.1 Die bestehende Systemarchitektur beurteilen und weiterentwickeln (Niveau: 3)
   * B12.3 Bestehende ICT Konfigurationen analysieren, Umsetzungsvarianten für die Erweiterung definieren und Soll-Konfigurationen entwickeln (Niveau: 3)
   * B12.4 Die Anforderungen an ein Konfigurationsmanagementsystem einer ICT-Organisation erheben und mögliche Lösungsvarianten vorschlagen (Niveau: 3)   

 * B14 Konzepte und Services umsetzen
   * B14.1 Technische und organisatorische Massnahmen planen und für die Einführung von Software bzw. Releases ausarbeiten (Niveau: 3)
   * B14.2 Probleme und Fehler im operativen Betrieb überwachen, identifizieren, zuordnen, beheben oder falls erforderlich eskalieren (Niveau: 3)  
   
 * C1 Cloud
   * C1.5 Der Studierende ist in der Lage Services für die Cloud bereitzustellen, diese zu automatisieren und jederzeit zur reproduzieren (Niveau: 4)

 * C2 Cloud-native 
   * C2.4 Container Cluster (Kubernetes) Umgebung aufsetzen und überwachen (Niveau: 3)
   

## Allgemeine Handlungskompetenzen

 * A2 Kommunikation situationsangepasst und wirkungsvoll gestalten
   * A2.1 Mündlich wie schriftlich sachlogisch, transparent und klar kommunizieren
   * A2.3 Quantität und Qualität der Informationen adressatengerecht selektieren und daraus folgend die Art und Form der Information festlegen
   * A2.5 Informations- und Kommunikationstechnologien (ICT) professionell einsetzen und etablieren
   * A2.6 Die branchenspezifischen Fachtermini des Engineerings verwenden und diese adressatengerecht kommunizieren

 * A3 Die persönliche Entwicklung reflektieren und aktiv gestalten
   * A3.1 Die eigenen Kompetenzen bezüglich der beruflichen Anforderungen regelmässig reflektieren, bewerten und daraus den Lernbedarf ermitteln
   * A3.2 Neues Wissen mit geeigneten Methoden erschliessen und arbeitsplatznahe Weiterbildung realisieren
   * A3.3 Neue Technologien kritisch reflexiv beurteilen, adaptieren und integrieren
   * A3.4 Die eigenen digitalen Kompetenzen kontinuierlich weiterentwickeln
   * A3.5 Das eigene Denken, Fühlen und Handeln reflektieren und geeignete persönliche Entwicklungsmassnahmen definieren und umsetzen      


## Anforderungsniveau

Das Anforderungsniveau einer Kompetenz ist durch die Komplexität der zu lösenden Problemstellung, die Veränderlichkeit und Unvorhersehbarkeit des Arbeitskontextes und die Verantwortlichkeit im Bereich der Zusammenarbeit und Führung definiert. HF Absolvierende sind generell in der Lage Problemstellungen und Herausforderungen zu analysieren, diese adäquat zu bewerten und mit innovativen Problemlösestrategien zu lösen. Die Handlungskompetenzen werden in vier Anforderungsniveaus eingestuft.

### Kompetenzniveau 1: Novizenkompetenz

Erfüllen selbständig fachliche Anforderungen; mehrheitlich wiederkehrende Aufgaben in einem überschaubaren und stabil strukturierten Tätigkeitsgebiet; Arbeit im Team und unter Anleitung.

### Kompetenzniveau 2: fortgeschrittene Kompetenz

Erkennen und analysieren umfassende fachliche Aufgabenstellungen in einem komplexen Arbeitskontext und sich veränderndem Arbeitsbereich; führen teils kleinere Teams; erledigen die Arbeiten selbständig unter Verantwortung einer Drittperson.

### Kompetenzniveau 3: Kompetenz professionellen Handelns

Bearbeiten neue komplexe Aufgaben und Problemstellungen in einem nicht vorhersehbaren Arbeitskontext; übernehmen die operative Verantwortung und planen, handeln und evaluieren autonom.

### Kompetenzniveau 4: Kompetenzexpertise

Entwickeln innovative Lösungen in einem komplexen Tätigkeitsfeld; antizipieren Veränderungen in der Zukunft und handeln proaktiv; übernehmen strategische Verantwortung und treiben Veränderungen und Entwicklungen voran.
